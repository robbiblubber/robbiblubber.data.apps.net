﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Linq;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Coding;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data.Interpreters;
using Robbiblubber.Data.Interpreters.History;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>This class implements the main window.</summary>
    public partial class FormMain: Form, ITargetWindow, IHistoryTargetWindow, IMenuTargetWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Current database provider.</summary>
        private ProviderItem _Provider = null;

        /// <summary>View mode.</summary>
        private ViewMode _View = ViewMode.TEXT;

        /// <summary>SQL interpreter.</summary>
        private SQLInterpreter _Interpreter;

        /// <summary>File name.</summary>
        private string _FileName = null;
        
        /// <summary>Last activated text box.</summary>
        private RichTextBox _CurrentRtf = null;

        /// <summary>Search in explorer.</summary>
        private bool _SearchExplorer = false;
        
        /// <summary>Result data.</summary>
        private List<Result> _ResultData;
        
        /// <summary>Result binding source.</summary>
        private BindingSource _BindingSource;
        
        /// <summary>Executing flag.</summary>
        private bool _Executing = false;
        
        /// <summary>Ask before aborting an execution.</summary>
        private bool _AskBeforeAbort = true;

        /// <summary>Recent file list.</summary>
        private RecentFileList _RecentFiles;

        /// <summary>Executing thread.</summary>
        private SynchronizedExecution _Exec = null;

        /// <summary>Show debug menu.</summary>
        private bool _ShowDebug = false;

        /// <summary>Execute on Alt+Enter.</summary>
        internal bool _ExecuteOnAltEnter = false;
        
        /// <summary>Execute on Ctrl+Enter.</summary>
        internal bool _ExecuteOnCtrlEnter = false;
        
        /// <summary>Automatically start AutoComplete.</summary>
        internal bool _AutoAutoStart = true;

        /// <summary>Toggle AutoComplete with Ctrl + Space.</summary>
        internal bool _AutoCtrlSpace = true;

        /// <summary>Complete words with space.</summary>
        internal bool _AutoApplySpace = true;

        /// <summary>Auto completion provider.</summary>
        private AutoCompletion _AutoCompletion = null;

        /// <summary>History auto completion provider.</summary>
        private AutoCompletion _AutoHistory = null;

        /// <summary>History auto completion source.</summary>
        private SQLHistoryAutoCompletionSource _AutoHistorySource;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private delegates                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Write delegate.</summary>
        /// <param name="text">Text.</param>
        private delegate void _Write(string text);


        /// <summary>Set data delegate.</summary>
        /// <param name="data">Data.</param>
        private delegate void _SetData(object data);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormMain()
        {
            ProviderManager.LoadProviders();

            InitializeComponent();

            __GridOut.Dock = DockStyle.Fill;
            __RtfOut.Dock = DockStyle.Fill;
            __RtfOut.BringToFront();
                        
            _Interpreter = new SQLInterpreter(this);

            _RecentFiles = new RecentFileList(PathOp.UserConfigurationPath + @"\recent.files", 6);
            _UpdateRecentFiles();

            this.RestoreLayout();
            View = (ViewMode) this.GetInteger("view");

            if(WorkspaceApplication._Program.SupportsExplorer)
            {
                ShowExplorer = this.GetBoolean("explorer", true);
                _SplitMain.RestoreLayout();
            }
            else
            {
                Text = "SQL Pad";
                Icon = Icon.FromHandle(new Bitmap(Resources.sqlpad).GetHicon());

                ShowExplorer = false;
                
                _MenuShowExplorer.Visible = false;
                _MenuViewBlank0.Visible = false;
                _MenuExplorer.Visible = false;
            }

            foreach(ISQLExport i in SQLExport.Modules)
            {
                try
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(i.MenuText);
                    m.Image = i.MenuIcon;
                    m.Tag = i;
                    m.Click += new EventHandler(_MenuExport_Click);

                    _MenuExport.DropDownItems.Add(m);
                }
                catch(Exception ex) { DebugOp.Dump("SQLFWX08140", ex); }
            }

            _CurrentRtf = __RtfIn;

            _SplitSQL.RestoreLayout();
            __RestoreLookAndFeel();

            Locale.UsingPrefixes("sqlfwx", "utctrl", "debugx");
            Locale.LoadSelection("Robbiblubber.Util.NET");
            this.Localize();

            __LoadSettings();

            try
            {
                if((!string.IsNullOrEmpty(WorkspaceApplication.__FileName) && File.Exists(WorkspaceApplication.__FileName)))
                {
                    __RtfIn.Text = File.ReadAllText(_FileName = WorkspaceApplication.__FileName);
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX08139", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the explorer tree is shown.</summary>
        public bool ShowExplorer
        {
            get { return (!_SplitMain.Panel1Collapsed); }
            set
            {
                _SplitMain.Panel1Collapsed = !(_MenuShowExplorer.Checked = value);
                _CheckDatabaseMenu();
            }
        }


        /// <summary>Gets or sets if the user will be asked befor an execution is aborted.</summary>
        public bool AskBeforeAbort
        {
            get { return _AskBeforeAbort; }
            set
            {
                _AskBeforeAbort = value;
                __SaveLookAndFeel();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal properties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the debug menu is shown.</summary>
        internal bool __ShowDebug
        {
            get { return _ShowDebug; }
            set { _ShowDebug = _MenuDebug.Visible = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Restores UI fonts and colors.</summary>
        internal void __RestoreLookAndFeel()
        {
            try
            {
                Ddp cfg = Ddp.Open(LayoutOp.FileName);

                __RtfIn.BackColor = ColorOp.Deserialze(cfg.Get<string>("/input/bg", __RtfIn.BackColor.Serialize()));
                __RtfIn.ForeColor = ColorOp.Deserialze(cfg.Get<string>("/input/text", __RtfIn.ForeColor.Serialize()));
                __RtfIn.Font = FontOp.Deserialize(cfg.Get<string>("/input/font", __RtfIn.Font.Serialize()));

                _ExecuteOnAltEnter = cfg.Get<bool>("/input/execute.alt", false);
                _ExecuteOnCtrlEnter = cfg.Get<bool>("/input/execute.ctrl", false);

                __RtfOut.BackColor = ColorOp.Deserialze(cfg.Get<string>("/output/bg", __RtfOut.BackColor.Serialize()));
                __RtfOut.ForeColor = ColorOp.Deserialze(cfg.Get<string>("/output/text", __RtfOut.ForeColor.Serialize()));
                __RtfOut.Font = FontOp.Deserialize(cfg.Get<string>("/output/font", __RtfOut.Font.Serialize()));

                _AskBeforeAbort = cfg.Get<bool>("/control/abort.ask", false);

                __GridOut.ColumnHeadersDefaultCellStyle.BackColor = __RtfOut.BackColor;
                __GridOut.RowHeadersDefaultCellStyle.BackColor = __RtfOut.BackColor;
                __GridOut.EnableHeadersVisualStyles = false;

                __GridOut.DefaultCellStyle.BackColor = __RtfOut.BackColor;
                __GridOut.BackgroundColor = __RtfOut.BackColor;
                __GridOut.ForeColor = __RtfOut.ForeColor;
                __GridOut.Font = __RtfOut.Font;
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0900", ex); }
        }


        /// <summary>Saves the UI fonts and colors.</summary>
        internal void __SaveLookAndFeel()
        {
            try
            {
                Ddp cfg = Ddp.Open(LayoutOp.FileName, GZip.BASE64);

                cfg.Set("/input/bg", __RtfIn.BackColor.Serialize());
                cfg.Set("/input/text", __RtfIn.ForeColor.Serialize());
                cfg.Set("/input/font", __RtfIn.Font.Serialize());
                cfg.Set("/input/execute.alt", _ExecuteOnAltEnter);
                cfg.Set("/input/execute.ctrl", _ExecuteOnCtrlEnter);

                cfg.Set("/output/bg", __RtfOut.BackColor.Serialize());
                cfg.Set("/output/text", __RtfOut.ForeColor.Serialize());
                cfg.Set("/output/font", __RtfOut.Font.Serialize());

                cfg.Set("/control/abort.ask", AskBeforeAbort);

                cfg.Save();

                __GridOut.ColumnHeadersDefaultCellStyle.BackColor = __RtfOut.BackColor;
                __GridOut.RowHeadersDefaultCellStyle.BackColor = __RtfOut.BackColor;
                __GridOut.EnableHeadersVisualStyles = false;

                __GridOut.DefaultCellStyle.BackColor = __RtfOut.BackColor;
                __GridOut.BackgroundColor = __RtfOut.BackColor;
                __GridOut.ForeColor = __RtfOut.ForeColor;
                __GridOut.Font = __RtfOut.Font;
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0901", ex); }
        }


        /// <summary>Saves the application settings.</summary>
        internal void __SaveSettings()
        {
            try
            {
                Locale.SaveSelection("Robbiblubber.Util.NET");

                Ddp cfg = Ddp.Open(PathOp.UserSettingsFile);

                cfg.Set("/settings/debug", __ShowDebug);

                cfg.Set("/autocomplete/autostart", _AutoAutoStart);
                cfg.Set("/autocomplete/ctrlspace", _AutoCtrlSpace);
                cfg.Set("/autocomplete/applyspace", _AutoApplySpace);

                cfg.Save();
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0902", ex); }
        }


        /// <summary>Loads the application settings.</summary>
        internal void __LoadSettings()
        {
            try
            {
                Ddp cfg = Ddp.Open(PathOp.UserSettingsFile);

                __ShowDebug = cfg.Get<bool>("/settings/debug", false);

                _AutoAutoStart = cfg.Get<bool>("/autocomplete/autostart", true);
                _AutoCtrlSpace = cfg.Get<bool>("/autocomplete/ctrlspace", true);
                _AutoApplySpace = cfg.Get<bool>("/autocomplete/applyspace", true);
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0903", ex); }
        }


        /// <summary>Appplies AutoComplete settings.</summary>
        internal void __ApplySettings()
        {
            if(_AutoCompletion != null)
            {
                _AutoCompletion.AutoStart = _AutoAutoStart;
                _AutoCompletion.CtrlSpace = _AutoCtrlSpace;
                _AutoCompletion.ApplySpace = _AutoApplySpace;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disconnects current provider.</summary>
        private void _Disconnect()
        {
            if(_Provider != null)
            {
                _Provider.Connection.Close();
                _Provider = null;
            }

            if(_AutoCompletion != null)
            {
                _AutoCompletion.Dispose();
                _AutoCompletion = null;
            }

            if(DatabaseMenus != null)
            {
                foreach(ToolStripItem i in DatabaseMenus)
                {
                    if(_MenuDatabase.DropDownItems.Contains(i)) { _MenuDatabase.DropDownItems.Remove(i); }
                }
                DatabaseMenus = null;
            }

            if(DropDownMenus != null)
            {
                foreach(ToolStripItem i in DropDownMenus)
                {
                    if(_CmenuTree.Items.Contains(i)) { _CmenuTree.Items.Remove(i); }
                }
                DropDownMenus = null;
            }

            if(MenuSelected != null)
            {
                foreach(Delegate i in MenuSelected.GetInvocationList())
                {
                    MenuSelected -= (MenuTargetEventHandler) i;
                }
            }

            if(MenuOpening != null)
            {
                foreach(Delegate i in MenuOpening.GetInvocationList())
                {
                    MenuOpening -= (MenuTargetEventHandler) i;
                }
            }

            if(MenuOpened != null)
            {
                foreach(Delegate i in MenuOpened.GetInvocationList())
                {
                    MenuOpened -= (MenuTargetEventHandler) i;
                }
            }

            if(MenuClosing != null)
            {
                foreach(Delegate i in MenuClosing.GetInvocationList())
                {
                    MenuClosing -= (MenuTargetEventHandler) i;
                }
            }

            if(MenuClosed != null)
            {
                foreach(Delegate i in MenuClosed.GetInvocationList())
                {
                    MenuClosed -= (MenuTargetEventHandler) i;
                }
            }

            if(TreeDoubleClick != null)
            {
                foreach(Delegate i in TreeDoubleClick.GetInvocationList())
                {
                    TreeDoubleClick -= (MenuTargetEventHandler) i;
                }
            }

            if(TreeKeyDown != null)
            {
                foreach(Delegate i in TreeKeyDown.GetInvocationList())
                {
                    TreeKeyDown -= (MenuTargetKeyEventHandler) i;
                }
            }

            if(TreeKeyUp != null)
            {
                foreach(Delegate i in TreeKeyUp.GetInvocationList())
                {
                    TreeKeyUp -= (MenuTargetKeyEventHandler) i;
                }
            }

            _CheckDatabaseMenu();
            Text = (WorkspaceApplication._Program.SupportsExplorer ? "rqoon" : "rqPad");
        }


        /// <summary>Sets the provider.</summary>
        /// <param name="p">Provider.</param>
        private void _SetProvider(ProviderItem p)
        {
            try
            {
                _Disconnect();

                p.Connect();
                _Provider = p;
                _Interpreter.ToggleTransactions();

                Text = (p.Name + (WorkspaceApplication._Program.SupportsExplorer ? " - SQL Workspace" : " - SQL Pad"));

                _TreeStructure.Nodes.Clear();
                if(p.Items != null)
                {
                    foreach(DbItem i in p.Items)
                    {
                        _DrawNode(i, _TreeStructure.Nodes);
                    }
                }

                _AutoCompletion = new AutoCompletion(__RtfIn, _Provider);
                __ApplySettings();
                _Provider.InitAutoCompletion(_AutoCompletion);

                DropDownMenus = new __MenuItemList();
                DatabaseMenus = new __MenuItemList();
                _Provider.InitMenus(this);
                _CheckDatabaseMenu();

                __RtfIn.Focus();
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX08141", ex);
                MessageBox.Show("sqlfwx::udiag.connect.failed".Localize("Failed to connect.") + ' ' + ex.Message, "sqlfwx::udiag.connect.caption".Localize("Connect"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        

        /// <summary>Adjusts database menu visibility.</summary>
        private void _CheckDatabaseMenu()
        {
            _MenuDatabase.Visible = ((_Provider != null) && ShowExplorer);
        }


        /// <summary>Aborts the current operation.</summary>
        /// <returns>Returns TRUE if the action has been aborted, otherwise returns FALSE.</returns>
        private bool _Abort()
        {
            if(!_Executing) { return true; }

            if(AskBeforeAbort)
            {
                if(MessageBox.Show("sqlfwx::udiag.abort.confirm".Localize("Do you really want to abort the current operation?"), "sqlfwx::udiag.abort.caption".Localize("Abort"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return false;
                }
            }

            try
            {
                if(_Exec != null) { /* TODO: _Exec.Abort(); */ }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX08142", ex); }

            return true;
        }


        /// <summary>Clears the output.</summary>
        private void _Clear()
        {
            try
            {
                __RtfOut.Clear();
                __GridOut.DataSource = null;
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0904", ex); }
        }


        /// <summary>Draws an item node.</summary>
        /// <param name="item">Item.</param>
        /// <param name="nodes">Parent node collection.</param>
        private void _DrawNode(DbItem item, TreeNodeCollection nodes)
        {
            try
            {
                if(!_IlistTree.Images.ContainsKey(item.IconKey))
                {
                    _IlistTree.Images.Add(item.IconKey, item.Icon);
                }

                TreeNode n = new TreeNode(item.Name);
                n.ImageKey = n.SelectedImageKey = item.IconKey;
                n.Tag = item;

                if(item.HasChildren)
                {
                    TreeNode m = new TreeNode("...");
                    m.ImageKey = m.SelectedImageKey = "ico::hourglass";

                    n.Nodes.Add(m);
                }

                nodes.Add(n);
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0905", ex); }
        }


        /// <summary>Refreshes a node.</summary>
        /// <param name="n">Tree node.</param>
        private void _Refresh(TreeNode n = null)
        {
            try
            {
                if(n == null) { n = _TreeStructure.SelectedNode; }
                if(n == null) return;

                _Refresh2(n);

                if((n.Nodes.Count > 0) && (n.Nodes[0].ImageKey == "ico::hourglass")) return;
                
                List<DbItem> exp = new List<DbItem>();
                _FindExpanded(exp, n);

                ((DbItem) n.Tag).Refresh();

                if(((DbItem) n.Tag).HasChildren)
                {
                    n.Nodes.Clear();
                    TreeNode m = new TreeNode("...");
                    m.ImageKey = m.SelectedImageKey = "ico::hourglass";

                    n.Nodes.Add(m);
                }

                _ExpandNeeded(exp, n);
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0906", ex); }
        }


        /// <summary>Refreshes node data sources.</summary>
        /// <param name="n">Node.</param>
        private void _Refresh2(TreeNode n)
        {
            if(n.Tag is DbItem) { ((DbItem) n.Tag).Refresh(); }

            foreach(TreeNode i in n.Nodes) _Refresh2(i);
        }


        /// <summary>Find expanded nodes.</summary>
        /// <param name="l">List.</param>
        /// <param name="n">Tree node.</param>
        private void _FindExpanded(List<DbItem> l, TreeNode n)
        {
            if(n.IsExpanded) { l.Add((DbItem) n.Tag); }
            foreach(TreeNode i in n.Nodes) { _FindExpanded(l, i); }
        }


        /// <summary>Expands nodes that needs to be expanded.</summary>
        /// <param name="l">List.</param>
        /// <param name="n">Tree node.</param>
        private void _ExpandNeeded(List<DbItem> l, TreeNode n)
        {
            foreach(DbItem i in l)
            {
                if(n.Tag == null) continue;

                if((((DbItem) n.Tag).Name == i.Name) && (n.Tag.GetType() == i.GetType()))
                {
                    _TreeStructure_BeforeExpand(_TreeStructure, new TreeViewCancelEventArgs(n, false, TreeViewAction.Expand));
                    n.Expand();
                    foreach(TreeNode j in n.Nodes) { _ExpandNeeded(l, j); }
                }
            }
        }


        /// <summary>Finds a text.</summary>
        /// <param name="f">String to find.</param>
        /// <param name="stage">Stage.</param>
        /// <param name="replace">Search only replaceable text.</param>
        private void _Find(string f, int stage = 0, bool replace = false)
        {
            try
            {
                RichTextBox rtf = _CurrentRtf;
                if(stage == 1) { rtf = ((rtf == __RtfIn) ? __RtfOut : __RtfIn); }

                f = f.ToLower();
                int v = -1;

                if(!(replace && (rtf != __RtfIn)))
                {
                    if((rtf == __RtfOut) && (View == ViewMode.GRID))
                    {
                        int rstart = ((stage == 0) ? (__GridOut.CurrentCell.RowIndex + 1) : 0);
                        int cstart = ((stage == 0) ? (__GridOut.CurrentCell.ColumnIndex + 1) : 0);

                        for(int r = rstart; r < __GridOut.RowCount; r++)
                        {
                            cstart = 0;
                            for(int c = cstart; c < __GridOut.ColumnCount; c++)
                            {
                                if(__GridOut[c, r].Value == null) continue;

                                if(__GridOut[c, r].Value.ToString().ToLower().Contains(f))
                                {
                                    __GridOut.ClearSelection();
                                    __GridOut[c, r].Selected = true;

                                    if(_FindSearch.Visible)
                                    {
                                        _FindSearch.Focus();
                                    }
                                    else { __GridOut.Focus(); }

                                    _CurrentRtf = __RtfOut;
                                    return;
                                }
                            }
                        }
                    }
                    else try
                        {
                            v = rtf.Text.ToLower().IndexOf(f, (stage == 0) ? rtf.SelectionStart + 1 : 0);
                        }
                        catch(Exception ex) { DebugOp.Dump("SQLFWX08142", ex); }
                }

                if(v < 0)
                {
                    if(++stage > 2)
                    {
                        MessageBox.Show("sqlfwx::udiag.find.nomatch".Localize("No matches."), "sqlfwx::udiag.find.caption".Localize("Find"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else { _Find(f, stage, replace); }

                    return;
                }

                rtf.SelectionStart = v;
                rtf.SelectionLength = f.Length;
                rtf.ScrollToCaret();
                _CurrentRtf = rtf;

                if(_FindSearch.Visible)
                {
                    _FindSearch.Focus();
                }
                else { rtf.Focus(); }
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0907", ex); }
        }


        /// <summary>Finds an item in the explorer.</summary>
        /// <param name="text">Text.</param>
        private void _FindExplorer(string text)
        {
            try
            {
                if(_TreeStructure.SelectedNode == null) { _TreeStructure.SelectedNode = _TreeStructure.Nodes[0]; }

                DbItem result = Provider.FindNext((DbItem) _TreeStructure.SelectedNode.Tag, text);

                if(result == null)
                {
                    MessageBox.Show("sqlfwx::udiag.find.nomatch".Localize("No matches."), "sqlfwx::udiag.find.caption".Localize("Find"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                _TreeStructure.SelectedNode = _NodeFromObject(_TreeStructure.Nodes, result);
                _TreeStructure.SelectedNode.EnsureVisible();
                _TreeStructure.Focus();
            }
            catch(Exception ex)
            {
                DebugOp.DumpMessage("UTLQLL00143", ex);
            }
        }


        /// <summary>Gets a tree node for an object.</summary>
        /// <param name="nodes">Node collection.</param>
        /// <param name="item">Item.</param>
        /// <returns></returns>
        private TreeNode _NodeFromObject(TreeNodeCollection nodes, DbItem item)
        {
            try
            {
                foreach(TreeNode i in nodes)
                {
                    if(i.Tag == item) return i;
                }

                DbItem anc = item.Parent;

                while(anc != null)
                {
                    foreach(TreeNode i in nodes)
                    {
                        if(i.Tag == anc)
                        {
                            _TreeStructure_BeforeExpand(_TreeStructure, new TreeViewCancelEventArgs(i, false, TreeViewAction.Expand));
                            return _NodeFromObject(i.Nodes, item);
                        }
                    }

                    anc = anc.Parent;
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0908", ex); }

            return null;
        }


        /// <summary>Updates recent files list.</summary>
        private void _UpdateRecentFiles()
        {
            try
            {
                _MenuRecent.DropDownItems.Clear();

                foreach(string i in _RecentFiles)
                {
                    Image img = Resources.document;

                    if(i.ToLower().EndsWith(".sql"))
                    {
                        img = Resources.sqldoc;
                    }
                    else if(i.ToLower().EndsWith(".dml"))
                    {
                        img = Resources.dmldoc;
                    }
                    if(i.ToLower().EndsWith(".ddl"))
                    {
                        img = Resources.ddldoc;
                    }

                    ToolStripMenuItem m = new ToolStripMenuItem(Path.GetFileName(i), img, new EventHandler(_MenuRecent_Click));
                    m.Tag = i;
                    m.ToolTipText = i;

                    if(_FileName != null) { m.Enabled = (Path.GetFullPath(i) != Path.GetFullPath(_FileName)); }

                    _MenuRecent.DropDownItems.Add(m);
                }

                _MenuRecent.Enabled = (_MenuRecent.DropDownItems.Count > 0);
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX0909", ex); }
        }


        /// <summary>Executes SQL.</summary>
        /// <param name="sql">SQL.</param>
        private void _Execute(string sql)
        {
            try
            {
                if(_Executing) return;

                if(_AutoCompletion != null) { _AutoCompletion.Hide(); }

                _Executing = true;
                __RtfOut.Clear();

                _WaitMain.Show("sqlfwx::udiag.wait.executing".Localize("Executing..."));

                _Interpreter.Source = sql;
                ExecuteSynchronized(new ThreadStart(_Interpreter.Execute));
                _MenuAbort.Enabled = false;

                if(_Interpreter.IsDDL)
                {
                    _WaitMain.Show("sqlfwx::udiag.wait.refreshing".Localize("Refreshing explorer..."));
                    foreach(TreeNode i in _TreeStructure.Nodes) { _Refresh(i); }
                }
                _MenuAbort.Enabled = true;
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX0909", ex); }

            _Executing = false;
            _WaitMain.Hide();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu "Databases" click.</summary>
        private void _MenuDatabases_Click(object sender, EventArgs e)
        {
            FormDatabases f = new FormDatabases();

            if(f.ShowDialog() == DialogResult.OK)
            {
                _SetProvider(f.Provider);
            }
        }


        /// <summary>Recent file click.</summary>
        private void _MenuRecent_Click(object sender, EventArgs e)
        {
            __RtfIn.Text = File.ReadAllText(_FileName = (string) ((ToolStripMenuItem) sender).Tag);

            _RecentFiles.Add(_FileName);
            _UpdateRecentFiles();
        }


        /// <summary>Menu "Connect" click.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MenuConnect_Click(object sender, EventArgs e)
        {
            FormConnect f = new FormConnect();

            try
            {
                if(f.ShowDialog() == DialogResult.OK)
                {
                    _SetProvider(f.Provider);
                }
            } catch(Exception) {}
        }


        /// <summary>Menu "View text" click.</summary>
        private void _MenuText_Click(object sender, EventArgs e)
        {
            View = ViewMode.TEXT;
        }


        /// <summary>Menu "View table" click.</summary>
        private void _MenuTable_Click(object sender, EventArgs e)
        {
            View = ViewMode.GRID;
        }


        /// <summary>Menu "Execute" click.</summary>
        private void _MenuExecute_Click(object sender, EventArgs e)
        {
            if(_Provider == null) return;
            _Execute(__RtfIn.Text);
        }


        /// <summary>Menu "Execute Current" click.</summary>
        private void _MenuExecuteCurrent_Click(object sender, EventArgs e)
        {
            if(_Provider == null) return;
            _Execute(_Provider.Parser.CurrentStatement(__RtfIn));
        }


        /// <summary>Menu "Execute to Current" click.</summary>
        private void _MenuExecuteToCurrent_Click(object sender, EventArgs e)
        {
            if(_Provider == null) return;
            _Execute(_Provider.Parser.ParseToCurrent(__RtfIn));
        }


        /// <summary>Menu "Execute From Current" click.</summary>
        private void _MenuEexecuteFromCurrent_Click(object sender, EventArgs e)
        {
            if(_Provider == null) return;
            _Execute(_Provider.Parser.ParseFromCurrent(__RtfIn));
        }


        /// <summary>Menu "Undo" click.</summary>
        private void _MenuUndo_Click(object sender, EventArgs e)
        {
            __RtfIn.Undo();
        }


        /// <summary>Menu "Cut" click.</summary>
        private void _MenuCut_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(__RtfIn.SelectedText.Replace("\r\n", "\n").Replace("\n", "\r\n"));
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX08144", ex);
                Clipboard.Clear();
            }

            __RtfIn.SelectedText = "";
        }


        /// <summary>Menu "Copy" click.</summary>
        private void _MenuCopy_Click(object sender, EventArgs e)
        {
            Control c = this.GetFocusControl();

            try
            {
                if(c == __RtfIn)
                {
                    Clipboard.SetText(__RtfIn.SelectedText.Replace("\r\n", "\n").Replace("\n", "\r\n"));
                }
                else if(c == __RtfOut)
                {
                    Clipboard.SetText(__RtfOut.SelectedText.Replace("\r\n", "\n").Replace("\n", "\r\n"));
                }
                else if(c == __GridOut)
                {
                    if(__GridOut.SelectedCells.Count > 0) { Clipboard.SetText(__GridOut.SelectedCells[0].Value.ToString()); }
                }
                else if(c == _TreeStructure)
                {
                    if(_TreeStructure.SelectedNode != null) { Clipboard.SetText(_TreeStructure.SelectedNode.Text); }
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX08145", ex); }
        }


        /// <summary>Menu "Paste" click.</summary>
        private void _MenuPaste_Click(object sender, EventArgs e)
        {
            __RtfIn.SelectedText = Clipboard.GetText();
        }


        /// <summary>Menu "Select all" click.</summary>
        private void _MenuSelectAll_Click(object sender, EventArgs e)
        {
            Control c = this.GetFocusControl();

            if(c == _FindSearch)
            {
                _FindSearch.SelectAll();
            }
            else if(c == _ReplaceSearch)
            {
                _ReplaceSearch.SelectAll();
            }
            else if(c == _GotoSearch)
            {
                _GotoSearch.SelectAll();
            }
            else if(c == __RtfIn)
            {
                __RtfIn.SelectAll();
            }
            else if(c == __RtfOut)
            {
                __RtfOut.SelectAll();
            }
            else if(c == __GridOut)
            {
                __GridOut.SelectAll();
            }
        }


        /// <summary>Menu "Deselect all" click.</summary>
        private void _MenuDeselectAll_Click(object sender, EventArgs e)
        {
            Control c = this.GetFocusControl();

            if(c == _FindSearch)
            {
                _FindSearch.SelectAll();
            }
            else if(c == _ReplaceSearch)
            {
                _ReplaceSearch.SelectAll();
            }
            else if(c == _GotoSearch)
            {
                _GotoSearch.SelectAll();
            }
            else if(c == __RtfIn)
            {
                __RtfIn.SelectionLength = 0;
            }
            else if(c == __RtfOut)
            {
                __RtfOut.SelectionLength = 0;
            }
            else if(c == __GridOut)
            {
                __GridOut.ClearSelection();
            }
        }


        /// <summary>Menu "Redo" click.</summary>
        private void _MenuRedo_Click(object sender, EventArgs e)
        {
            __RtfIn.Redo();
        }


        /// <summary>Menu "Save" click.</summary>
        private void _MenuSave_Click(object sender, EventArgs e)
        {
            if(_FileName == null)
            {
                _MenuSaveAs_Click(sender, e);
                return;
            }

            try
            {
                File.WriteAllText(_FileName, __RtfIn.Text);

                _RecentFiles.Add(_FileName);
                _UpdateRecentFiles();
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX08146", ex);
                MessageBox.Show("sqlfwx::udiag.save.fail".Localize("Failed to save file.") + ' ' + ex.Message, "sqlfwx::udiag.save.fail".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Save as" click.</summary>
        private void _MenuSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "sqlfwx::udiag.filter.sql".Localize("SQL Files") + " (*.sql)|*.sql|" + "sqlfwx::udiag.filter.dml".Localize("DML Files") + " (*.dml)|*.dml|" + "sqlfwx::udiag.filter.ddl".Localize("DDL Files") + " (*.ddl)|*.ddl|" + "sqlfwx::udiag.filter.all".Localize("All files") + "|*.*";

            if(f.ShowDialog() == DialogResult.OK)
            {
                _FileName = f.FileName;
                _MenuSave_Click(sender, e);
            }
        }



        /// <summary>Menu "Open" click.</summary>
        private void _MenuOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "sqlfwx::udiag.filter.rdbs".Localize("RDBMS Script Files") + " (*.sql, *.dml, *.ddl)|*.sql;*.dml;*.ddl|" + "sqlfwx::udiag.filter.all".Localize("All files") + "|*.*";

            if(f.ShowDialog() == DialogResult.OK)
            {
                _FileName = f.FileName;

                try
                {
                    __RtfIn.Text = File.ReadAllText(_FileName);

                    _RecentFiles.Add(_FileName);
                    _UpdateRecentFiles();
                }
                catch(Exception ex)
                {
                    DebugOp.Dump("SQLFWX08147", ex);
                    MessageBox.Show("sqlfwx::udiag.open.fail".Localize("Failed to open file.") + ' ' + ex.Message, "sqlfwx::udiag.open.caption".Localize("Open"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        /// <summary>Menu "Clear" click.</summary>
        private void _MenuClear_Click(object sender, EventArgs e)
        {
            __RtfIn.Clear();
            _Clear();
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        /// <summary>Menu "Edit" opening.</summary>
        private void _MenuEdit_DropDownOpening(object sender, EventArgs e)
        {
            if(_Executing) return;

            _MenuPaste.Enabled = _MenuCut.Enabled = (this.GetFocusControl() == __RtfIn);
            _MenuUndo.Enabled = __RtfIn.CanUndo;
            _MenuRedo.Enabled = __RtfIn.CanRedo;
        }


        /// <summary>Menu "Help" click.</summary>
        private void _MenuShowHelp_Click(object sender, EventArgs e)
        {
            if(WorkspaceApplication._Program.SupportsExplorer)
            {
                Process.Start("http://robbiblubber.lima-city.de/wiki.php?title=Help-SQLWS");
            }
            else
            {
                Process.Start("http://robbiblubber.lima-city.de/wiki.php?title=Help-SQLPad");
            }
        }


        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, EventArgs e)
        {
            WorkspaceApplication._Program.ShowAbout();
        }


        /// <summary>Menu "Explorer" click.</summary>
        private void _MenuShowExplorer_Click(object sender, EventArgs e)
        {
            ShowExplorer = (!ShowExplorer);
        }
        

        /// <summary>Menu "Disconnect" click.</summary>
        private void _MenuDisconnect_Click(object sender, EventArgs e)
        {
            _Disconnect();
            _TreeStructure.Nodes.Clear();
            _Clear();
        }


        /// <summary>Before expland tree.</summary>
        private void _TreeStructure_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if((e.Node.Nodes.Count > 0) && (e.Node.Nodes[0].ImageKey == "ico::hourglass"))
            {
                e.Node.Nodes.Clear();
                
                foreach(DbItem i in ((DbItem) e.Node.Tag).Children)
                {
                    _DrawNode(i, e.Node.Nodes);
                }
            }
        }


        /// <summary>After select tree node.</summary>
        private void _TreeStructure_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(((DbItem) e.Node.Tag).DisplayText))
            {
                View = ViewMode.TEXT;
                __RtfOut.Text = (((DbItem) e.Node.Tag).DisplayText);
            }
        }


        /// <summary>Context menu opening.</summary>
        private void _CmenuTree_Opening(object sender, CancelEventArgs e)
        {
            if((_TreeStructure.SelectedNode == null) || (!(_TreeStructure.SelectedNode.Tag is DbItem)))
            {
                e.Cancel = true;
                return;
            }

            _CmenuDisconnect.Visible = _CmenuTreeBlank0.Visible = (_TreeStructure.SelectedNode == _TreeStructure.Nodes[0]);

            if(((__MenuItemList) DropDownMenus).Changed)
            {
                foreach(ToolStripItem i in ((__MenuItemList) DropDownMenus).ItemsRemoved.Union(DropDownMenus))
                {
                    if(_CmenuTree.Items.Contains(i))
                    {
                        _CmenuTree.Items.Remove(i);
                        i.Click -= _CmenuTree_MenuSelected;
                    }
                }

                int n = 2;
                foreach(ToolStripItem i in DropDownMenus) 
                {
                    bool top = true;
                    foreach(ToolStripItem k in DropDownMenus)
                    {
                        if(k is ToolStripMenuItem)
                        {
                            if(((ToolStripMenuItem) k).DropDownItems.Contains(i)) { top = false; break; }
                        }
                    }

                    if(top) { _CmenuTree.Items.Insert(n++, i); }
                    if(i is ToolStripMenuItem)  { i.Click += _CmenuTree_MenuSelected;  }
                }

                ((__MenuItemList) DropDownMenus).AcknowledgeChanges();
            }

            MenuOpening?.Invoke(DropDownMenus, new MenuTargetEventArgs(SelectedItem, SelectedNode));
        }


        /// <summary>Menu "Database" opening.</summary>
        private void _MenuDatabase_DropDownOpening(object sender, EventArgs e)
        {
            if(DatabaseMenus == null)
            {
                return;
            }

            if(((__MenuItemList) DatabaseMenus).Changed)
            {
                foreach(ToolStripItem i in ((__MenuItemList) DatabaseMenus).ItemsRemoved.Union(DatabaseMenus))
                {
                    if(_MenuDatabase.DropDownItems.Contains(i))
                    {
                        _MenuDatabase.DropDownItems.Remove(i);
                        i.Click -= _CmenuTree_MenuSelected;
                    }
                }

                int n = 0;
                foreach(ToolStripItem i in DatabaseMenus)
                {
                    bool top = true;
                    foreach(ToolStripItem k in DatabaseMenus)
                    {
                        if(k is ToolStripMenuItem)
                        {
                            if(((ToolStripMenuItem) k).DropDownItems.Contains(i)) { top = false; break; }
                        }
                    }

                    if(top) { _MenuDatabase.DropDownItems.Insert(n++, i); }
                    if(i is ToolStripMenuItem) { i.Click += _CmenuTree_MenuSelected; }
                }

                ((__MenuItemList) DatabaseMenus).AcknowledgeChanges();
            }

            MenuOpening?.Invoke(DatabaseMenus, new MenuTargetEventArgs(SelectedItem, SelectedNode));
        }


        /// <summary>Custom menu clicked.</summary>
        private void _CmenuTree_MenuSelected(object sender, EventArgs e)
        {
            MenuSelected?.Invoke(sender, new MenuTargetEventArgs((DbItem) _TreeStructure.SelectedNode.Tag, _TreeStructure.SelectedNode));
        }


        /// <summary>Context menu opened.</summary>
        private void _CmenuTree_Opened(object sender, EventArgs e)
        {
            MenuOpened?.Invoke(DropDownMenus, new MenuTargetEventArgs((DbItem) _TreeStructure.SelectedNode.Tag, _TreeStructure.SelectedNode));
        }


        /// <summary>Context menu closing.</summary>
        private void _CmenuTree_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            MenuClosing?.Invoke(DropDownMenus, new MenuTargetEventArgs((DbItem) _TreeStructure.SelectedNode.Tag, _TreeStructure.SelectedNode));
        }


        /// <summary>Context menu closed.</summary>
        private void _CmenuTree_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            MenuClosed?.Invoke(DropDownMenus, new MenuTargetEventArgs((DbItem) _TreeStructure.SelectedNode.Tag, _TreeStructure.SelectedNode));
        }


        /// <summary>Tree view mouse down.</summary>
        private void _TreeStructure_MouseDown(object sender, MouseEventArgs e)
        {
            _TreeStructure.SelectedNode = _TreeStructure.HitTest(e.Location).Node;
        }


        /// <summary>Menu "Refresh" click.</summary>
        private void _CmenuRefresh_Click(object sender, EventArgs e)
        {
            _Refresh();
        }


        /// <summary>Menu "Browse history" click.</summary>
        private void _MenuBrowseHistory_Click(object sender, EventArgs e)
        {
            SQLHistory.BrowseHistory(this);
        }


        /// <summary>Menu "Dump history" click.</summary>
        private void _MenuDumpHistory_Click(object sender, EventArgs e)
        {
            SQLHistory.DumpHistory(this);
        }


        /// <summary>Menu "Favorite SQL" click.</summary>
        private void _MenuFavoriteSQL_Click(object sender, EventArgs e)
        {
            SQLHistory.BrowseHistory(this, true);
        }


        /// <summary>Menu "Clean up history" click.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MenuCleanUpHistory_Click(object sender, EventArgs e)
        {
            SQLHistory.CleanupHistory(this);
        }

        
        /// <summary>Any "Export" menu click.</summary>
        private void _MenuExport_Click(object sender, EventArgs e)
        {
            if(((ToolStripMenuItem) sender).Tag is ISQLExport)
            {
                _Interpreter.Source = __RtfIn.Text;
                ((ISQLExport) ((ToolStripMenuItem) sender).Tag).Export(_Interpreter);
            }
        }


        /// <summary>Manu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            ISettingsWindow f = Settings.CreateDialog();

            ((GeneralSettingsControl) f.GetControl("settings::general")).InitSettings(_Interpreter);
            ((EnvironmentSettingsControl) f.GetControl("settings::env")).InitSettings(this);
            ((TextEditorSettingsControl) f.GetControl("settings::editor")).InitSettings(this);

            if(f.ShowDialog() == DialogResult.OK)
            {
                ((GeneralSettingsControl) f.GetControl("settings::general")).ApplySettings(_Interpreter);
                ((EnvironmentSettingsControl) f.GetControl("settings::env")).ApplySettings(this);
                ((TextEditorSettingsControl) f.GetControl("settings::editor")).ApplySettings(this);

                _MenuConsole_DropDownOpening(null, null);
            }
        }


        /// <summary>Menu "Console" opening.</summary>
        private void _MenuConsole_DropDownOpening(object sender, EventArgs e)
        {
            _MenuExecute.Visible = _MenuExecuteCurrent.Visible = _MenuEexecuteFromCurrent.Visible = _MenuExecuteToCurrent.Visible = (!_Executing);
            _MenuAbort.Visible = _Executing;
            _MenuConsoleBlank0.Visible = _MenuCommit.Visible = _MenuRollback.Visible = _Interpreter.TransactionsActive;

            _MenuExecute.Enabled = _MenuExecuteCurrent.Enabled = _MenuEexecuteFromCurrent.Enabled = _MenuExecuteToCurrent.Enabled =
                                   _MenuCommit.Enabled = _MenuRollback.Enabled = (_Provider != null);
        }


        /// <summary>Menu "Console" closed.</summary>
        private void _MenuConsole_DropDownClosed(object sender, EventArgs e)
        {
            _MenuExecute.Visible = _MenuExecuteCurrent.Visible = _MenuEexecuteFromCurrent.Visible = _MenuExecuteToCurrent.Visible = _MenuAbort.Visible = true;
            _MenuConsoleBlank0.Visible = _MenuCommit.Visible = _MenuRollback.Visible = true;

            _MenuExecute.Enabled = _MenuExecuteCurrent.Enabled = _MenuEexecuteFromCurrent.Enabled = _MenuExecuteToCurrent.Enabled =
                                   _MenuCommit.Enabled = _MenuRollback.Enabled = true;
        }


        /// <summary>Menu "Tools" opening.</summary>
        private void _MenuTools_DropDownOpening(object sender, EventArgs e)
        {
            _MenuExport.Enabled = _MenuExplorer.Enabled = (_Provider != null);
        }

        
        /// <summary>Menu "Tools" closing.</summary>
        private void _MenuTools_DropDownClosed(object sender, EventArgs e)
        {
            _MenuExport.Enabled = _MenuExplorer.Enabled = true;
        }


        /// <summary>Menu "Commit" click.</summary>
        private void _MenuCommit_Click(object sender, EventArgs e)
        {
            if(!_Interpreter.TransactionsActive) return;
            _Interpreter.Commit();
        }

        
        /// <summary>Menu "Rollback" click.</summary>
        private void _MenuRollback_Click(object sender, EventArgs e)
        {
            if(!_Interpreter.TransactionsActive) return;
            _Interpreter.Rollback();
        }


        /// <summary>Window closing.</summary>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(_Executing)
            {
                if(!_Abort())
                {
                    e.Cancel = true;
                    return;
                }
            }

            this.SaveLayout();
            this.SetData("view", (int) View);

            _SplitSQL.SaveLayout();

            try
            {
                if(_Provider != null)
                {
                    _Provider.Disconnect();
                }
            }
            catch(Exception ex) { DebugOp.Dump("SQLFWX08148", ex); }
        }


        /// <summary>Input window key down.</summary>
        private void __RtfIn_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if((_ExecuteOnAltEnter && e.Alt) || (_ExecuteOnCtrlEnter && e.Control)) { _MenuExecute_Click(null, null); e.Handled = true; }

                if(e.Control)
                {
                    if(_AutoCompletion.Visible) { _AutoCompletion.Hide(); }
                    if(_AutoHistory == null)
                    {
                        _AutoHistory = new AutoCompletion(__RtfIn, _AutoHistorySource = new SQLHistoryAutoCompletionSource(this));
                        _AutoHistory.AutoStart = false;
                    }
                    _AutoHistorySource.OnlyFavorites = e.Shift;
                    _AutoHistory.Show(false);
                    e.Handled = true;
                }
            }
        }


        /// <summary>Goto go to.</summary>
        private void _GotoSearch_GoTo(object sender, EventArgs e)
        {
            int tar = __RtfIn.GetFirstCharIndexFromLine(_GotoSearch.Line - 1);

            if(tar > -1)
            {
                __RtfIn.Select(tar, 0);
            }
            else
            {
                __RtfIn.Select(__RtfIn.Text.Length, 0);
            }
            __RtfIn.Focus();
        }


        /// <summary>Goto line changed.</summary>
        private void _GotoSearch_LineChanged(object sender, GoToFeedbackEventArgs e)
        {
            e.Allowed = ((e.Line > 0) && (e.Line <= __RtfIn.Lines.Length));
        }


        /// <summary>Menu "Go to" click.</summary>
        private void _MenuGoTo_Click(object sender, EventArgs e)
        {
            _FindSearch.Hide();
            _ReplaceSearch.Hide();

            _GotoSearch.Line = (__RtfIn.GetLineFromCharIndex(__RtfIn.SelectionStart) + 1);
            _GotoSearch.Show();
            _GotoSearch.BringToFront();
        }


        /// <summary>Menu "Find" click.</summary>
        private void _MenuFind_Click(object sender, EventArgs e)
        {
            _SearchExplorer = false;

            _GotoSearch.Hide();
            _ReplaceSearch.Hide();
            _FindSearch.Show();
            _FindSearch.BringToFront();
        }


        /// <summary>Find click.</summary>
        private void _FindSearch_Search(object sender, EventArgs e)
        {
            if(_SearchExplorer)
            {
                _FindExplorer(FindControl.CurrentSearchText);
            }
            else { _Find(FindControl.CurrentSearchText); }

            _ReplaceSearch_SearchTextChanged(null, null);
        }


        /// <summary>Menu "Find next" click.</summary>
        private void _MenuFindNext_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(_FindSearch.SearchText))
            {
                _MenuFind_Click(sender, e);
            }
            else
            {
                _Find(FindControl.CurrentSearchText);
            }
        }


        /// <summary>Menu "Find next" (in Explorer) click.</summary>
        private void _MenuExplorerFindNext_Click(object sender, EventArgs e)
        {
            if(_Provider == null) return;

            if(string.IsNullOrEmpty(_FindSearch.SearchText))
            {
                _MenuExplorerFind_Click(sender, e);
            }
            else
            {
                _FindExplorer(FindControl.CurrentSearchText);
            }
        }


        /// <summary>Menu "Replace" click.</summary>
        private void _MenuReplace_Click(object sender, EventArgs e)
        {
            _GotoSearch.Hide();
            _FindSearch.Hide();
            _ReplaceSearch_SearchTextChanged(null, null);
            _ReplaceSearch.Show();
            _ReplaceSearch.BringToFront();
        }


        /// <summary>Replace search text changed.</summary>
        private void _ReplaceSearch_SearchTextChanged(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(_ReplaceSearch.SearchText)) { _ReplaceSearch.AllowReplaceAll = _ReplaceSearch.AllowReplace = false; return; }
            _ReplaceSearch.AllowReplaceAll = true;

            _ReplaceSearch.AllowReplace = (__RtfIn.SelectedText.ToLower() == _ReplaceSearch.SearchText.ToLower());
        }


        /// <summary>Button "Replace" click.</summary>
        private void _ReplaceSearch_Replace(object sender, EventArgs e)
        {
            __RtfIn.SelectedText = _ReplaceSearch.ReplaceText;
            _ReplaceSearch_Search(null, null);
        }


        /// <summary>Button "Replace all" click.</summary>
        private void _ReplaceSearch_ReplaceAll(object sender, EventArgs e)
        {
            __RtfIn.Text = __RtfIn.Text.ReplaceAll(_ReplaceSearch.SearchText, _ReplaceSearch.ReplaceText, false);
            _ReplaceSearch_SearchTextChanged(null, null);
        }


        /// <summary>Replace search next.</summary>
        private void _ReplaceSearch_Search(object sender, EventArgs e)
        {
            _Find(FindControl.CurrentSearchText, 0, true);
            _ReplaceSearch_SearchTextChanged(null, null);
        }


        /// <summary>Finds an object in the explorer.</summary>
        private void _MenuExplorerFind_Click(object sender, EventArgs e)
        {
            if(_Provider == null) return;

            _SearchExplorer = true;

            _GotoSearch.Hide();
            _ReplaceSearch.Hide();
            _FindSearch.Show();
            _FindSearch.BringToFront();
        }


        /// <summary>Input context menu opening.</summary>
        private void _CmenuIn_Opening(object sender, CancelEventArgs e)
        {
            _CmenuInCommit.Visible = _CmenuInRollback.Visible = _CmenuInBlank1.Visible = (_Interpreter.UseTransactions);
        }


        /// <summary>Menu "Abort" click.</summary>
        private void _MenuAbort_Click(object sender, EventArgs e)
        {
            if(!_Executing) return;
            _Abort();
        }


        /// <summary>Menu "File" opening.</summary>
        private void _MenuFile_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDisconnect.Enabled = (_Provider != null);
            _MenuSave.Enabled = (_FileName != null);
        }


        /// <summary>Menu "File" closed.</summary>
        private void _MenuFile_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDisconnect.Enabled = _MenuSave.Enabled = true;
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStop.Visible = (!(_MenuDebugStart.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true));
        }


        /// <summary>Menu "Start/Stop debugging" clicked.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save dump" clicked.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            SaveFileDialog d = new SaveFileDialog();
            d.Filter = "debugx::udiag.filter.dump".Localize("Debug dump files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All files") + "|*.*";

            if(d.ShowDialog() == DialogResult.OK)
            {
                DebugOp.CreateDumpFile(d.FileName);
            }
        }


        /// <summary>Menu "View dump" clicked.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            DebugOp.ShowLog();
        }


        /// <summary>Menu "Upload dump" clicked.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/Robbiblubber.Util.NET/");
        }


        /// <summary>Tree key down.</summary>
        private void _TreeStructure_KeyDown(object sender, KeyEventArgs e)
        {
            TreeKeyDown?.Invoke(sender, new MenuTargetKeyEventArgs(SelectedItem, SelectedNode, e));
        }


        /// <summary>Tree key up.</summary>
        private void _TreeStructure_KeyUp(object sender, KeyEventArgs e)
        {
            TreeKeyUp?.Invoke(sender, new MenuTargetKeyEventArgs(SelectedItem, SelectedNode, e));
        }


        /// <summary>Tree double click.</summary>
        private void _TreeStructure_DoubleClick(object sender, EventArgs e)
        {
            TreeDoubleClick?.Invoke(sender, new MenuTargetEventArgs(SelectedItem, SelectedNode));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITargetWindow                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the database provider.</summary>
        public ProviderItem Provider
        {
            get { return _Provider; }
        }


        /// <summary>Gets or sets the view mode.</summary>
        public ViewMode View
        {
            get { return _View; }
            set
            {
                if(_View == value) return;

                _View = value;

                _Clear();

                if(_View == ViewMode.TEXT)
                {
                    _MenuTable.Checked = false;
                    _MenuText.Checked = true;

                    __RtfOut.Visible = true;
                    __GridOut.Visible = false;
                    __RtfOut.BringToFront();
                }
                else
                {
                    _MenuText.Checked = false;
                    _MenuTable.Checked = true;

                    __GridOut.Visible = true;
                    __RtfOut.Visible = false;
                    __GridOut.BringToFront();
                }
            }
        }


        /// <summary>Gets the data grid.</summary>
        public DataGridView Grid
        {
            get { return __GridOut; }
        }


        /// <summary>Writes to console.</summary>
        /// <param name="text">Text.</param>
        public void Write(string text)
        {
            if(__RtfOut.InvokeRequired)
            {
                __RtfOut.Invoke(new _Write(Write), text);
            }
            else { __RtfOut.AppendText(text); }
        }


        /// <summary>Writes to grid.</summary>
        /// <param name="text">Text.</param>
        public void WriteToGrid(string text)
        {
            if(__GridOut.InvokeRequired)
            {
                __GridOut.Invoke(new _Write(WriteToGrid), text);
            }
            else
            {
                _ResultData = new List<Result>();
                _ResultData.Add(new Result(text));

                _BindingSource = new BindingSource();
                _BindingSource.DataSource = _ResultData;

                __GridOut.ReadOnly = true;
                __GridOut.DataSource = _BindingSource;
                __GridOut.Columns[0].HeaderText = "RESULT";
                __GridOut.Columns[0].Width = (__GridOut.Width - 64);

                __GridOut.ColumnHeadersDefaultCellStyle.Font = new Font(__GridOut.DefaultCellStyle.Font, FontStyle.Bold);
                __GridOut.EnableHeadersVisualStyles = false;
            }
        }


        /// <summary>Sets grid data source.</summary>
        /// <param name="dataSource">Data source.</param>
        public void DataToGrid(object dataSource)
        {
            if(__GridOut.InvokeRequired)
            {
                __GridOut.Invoke(new _SetData(DataToGrid), dataSource);
            }
            else
            {
                __GridOut.ReadOnly = true;
                __GridOut.DataSource = dataSource;
            }

            __GridOut.ColumnHeadersDefaultCellStyle.Font = new Font(__GridOut.DefaultCellStyle.Font, FontStyle.Bold);
            __GridOut.EnableHeadersVisualStyles = false;
        }


        /// <summary>Shows the wait window.</summary>
        public void ShowWait()
        {
            _WaitMain.Show("Executing...");
        }


        /// <summary>Shows the wait window.</summary>
        /// <param name="text">Text.</param>
        public void ShowWait(string text)
        {
            _WaitMain.Show(text);
        }


        /// <summary>Hides the wait window.</summary>
        public void HideWait()
        {
            _WaitMain.Hide();
        }


        /// <summary>Refreshes the tree view.</summary>
        /// <param name="n">Tree node.</param>
        public void RefreshTree(TreeNode n = null)
        {
            try
            {
                if(n == null) { n = _TreeStructure.SelectedNode.Parent; }
            }
            catch(Exception) {}

            _Refresh(n);
        }


        /// <summary>Gets the selected item.</summary>
        public DbItem SelectedItem 
        { 
            get
            {
                if(ShowExplorer && (_TreeStructure.SelectedNode != null))
                {
                    if(_TreeStructure.SelectedNode.Tag is DbItem) { return (DbItem) _TreeStructure.SelectedNode.Tag; }
                }

                return null;
            }
        }


        /// <summary>Gets the selected node.</summary>
        public TreeNode SelectedNode
        {
            get
            {
                if(ShowExplorer && (_TreeStructure.SelectedNode != null))
                {
                    if(_TreeStructure.SelectedNode.Tag is DbItem) { return _TreeStructure.SelectedNode; }
                }

                return null;
            }
        }


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        public void ExecuteSynchronized(ThreadStart c)
        {
            _Executing = true;
            _Exec = new SynchronizedExecution();
            _Exec.ExecuteSynchronized(c);
            _Executing = false;
        }


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Paramter.</param>
        public void ExecuteSynchronized(ParameterizedThreadStart c, object p)
        {
            _Executing = true;
            _Exec = new SynchronizedExecution();
            _Exec.ExecuteSynchronized(c, p);
            _Executing = false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITreeNodeMenu                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the menu is opening.</summary>
        public event MenuTargetEventHandler MenuOpening;


        /// <summary>Occurs when the menu is opened.</summary>
        public event MenuTargetEventHandler MenuOpened;


        /// <summary>Occurs when the menu is closing.</summary>
        public event MenuTargetEventHandler MenuClosing;


        /// <summary>Occurs when the menu is closed.</summary>
        public event MenuTargetEventHandler MenuClosed;


        /// <summary>Occors when a menu item is clicked.</summary>
        public event MenuTargetEventHandler MenuSelected;


        /// <summary>Occurs when a key down event is raised by the tree view.</summary>
        public event MenuTargetKeyEventHandler TreeKeyDown;


        /// <summary>Occurs when a key up event is raised by the tree view.</summary>
        public event MenuTargetKeyEventHandler TreeKeyUp;


        /// <summary>Occurs when a double click event is raised by the tree view.</summary>
        public event MenuTargetEventHandler TreeDoubleClick;


        /// <summary>Gets the drop down menu items associated with this provider.</summary>
        public IList<ToolStripItem> DropDownMenus
        { 
            get; private set;
        }


        /// <summary>Gets the database menu items associated with this provider.</summary>
        public IList<ToolStripItem> DatabaseMenus
        {
            get; private set;
        }


        /// <summary>Gets the target window.</summary>
        public ITargetWindow TargetWindow 
        { 
            get { return this; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IHistoryTargetWindow                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Applies a text from history.</summary>
        /// <param name="text">Text.</param>
        void IHistoryTargetWindow.Apply(string text)
        {
            __RtfIn.SelectedText = text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [strcut] Result                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class represents a result.</summary>
        public struct Result
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Value.</summary>
            private string _Value;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="value">Value.</param>
            public Result(string value)
            {
                _Value = value;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the value.</summary>
            public string Value
            {
                get { return _Value; }
            }
        }
    }
}
