﻿namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    partial class EnvironmentSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnvironmentSettingsControl));
            this._LabelAutoComplete = new System.Windows.Forms.Label();
            this._LabelAutoCompleteIcon = new System.Windows.Forms.Label();
            this._PanelAutoComplete = new System.Windows.Forms.Panel();
            this._CheckCompleteSapce = new System.Windows.Forms.CheckBox();
            this._CheckAutoStart = new System.Windows.Forms.CheckBox();
            this._CheckCompleteCtrlSpace = new System.Windows.Forms.CheckBox();
            this._LabelLangDebug = new System.Windows.Forms.Label();
            this._LabelLangDebugIcon = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._LabelLanguage = new System.Windows.Forms.Label();
            this._CheckDebug = new System.Windows.Forms.CheckBox();
            this._LcboLanguage = new Robbiblubber.Util.Localization.Controls.LocaleComboBox();
            this._PanelAutoComplete.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelAutoComplete
            // 
            this._LabelAutoComplete.AutoSize = true;
            this._LabelAutoComplete.Location = new System.Drawing.Point(44, 27);
            this._LabelAutoComplete.Name = "_LabelAutoComplete";
            this._LabelAutoComplete.Size = new System.Drawing.Size(96, 17);
            this._LabelAutoComplete.TabIndex = 0;
            this._LabelAutoComplete.Tag = "sqlfwx::udiag.envset.autoc";
            this._LabelAutoComplete.Text = "&Auto-Complete";
            // 
            // _LabelAutoCompleteIcon
            // 
            this._LabelAutoCompleteIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelAutoCompleteIcon.Image")));
            this._LabelAutoCompleteIcon.Location = new System.Drawing.Point(21, 26);
            this._LabelAutoCompleteIcon.Name = "_LabelAutoCompleteIcon";
            this._LabelAutoCompleteIcon.Size = new System.Drawing.Size(24, 21);
            this._LabelAutoCompleteIcon.TabIndex = 0;
            // 
            // _PanelAutoComplete
            // 
            this._PanelAutoComplete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelAutoComplete.Controls.Add(this._CheckCompleteSapce);
            this._PanelAutoComplete.Controls.Add(this._CheckAutoStart);
            this._PanelAutoComplete.Controls.Add(this._CheckCompleteCtrlSpace);
            this._PanelAutoComplete.Location = new System.Drawing.Point(24, 50);
            this._PanelAutoComplete.Name = "_PanelAutoComplete";
            this._PanelAutoComplete.Size = new System.Drawing.Size(651, 108);
            this._PanelAutoComplete.TabIndex = 0;
            // 
            // _CheckCompleteSapce
            // 
            this._CheckCompleteSapce.AutoSize = true;
            this._CheckCompleteSapce.Location = new System.Drawing.Point(22, 65);
            this._CheckCompleteSapce.Name = "_CheckCompleteSapce";
            this._CheckCompleteSapce.Size = new System.Drawing.Size(178, 21);
            this._CheckCompleteSapce.TabIndex = 5;
            this._CheckCompleteSapce.Tag = "sqlfwx::udiag.envset.spccomp";
            this._CheckCompleteSapce.Text = "   Space completes &words";
            this._CheckCompleteSapce.UseVisualStyleBackColor = true;
            // 
            // _CheckAutoStart
            // 
            this._CheckAutoStart.AutoSize = true;
            this._CheckAutoStart.Location = new System.Drawing.Point(22, 16);
            this._CheckAutoStart.Name = "_CheckAutoStart";
            this._CheckAutoStart.Size = new System.Drawing.Size(146, 21);
            this._CheckAutoStart.TabIndex = 3;
            this._CheckAutoStart.Tag = "sqlfwx::udiag.envset.autostart";
            this._CheckAutoStart.Text = "   &Start automatically";
            this._CheckAutoStart.UseVisualStyleBackColor = true;
            // 
            // _CheckCompleteCtrlSpace
            // 
            this._CheckCompleteCtrlSpace.AutoSize = true;
            this._CheckCompleteCtrlSpace.Location = new System.Drawing.Point(22, 40);
            this._CheckCompleteCtrlSpace.Name = "_CheckCompleteCtrlSpace";
            this._CheckCompleteCtrlSpace.Size = new System.Drawing.Size(256, 21);
            this._CheckCompleteCtrlSpace.TabIndex = 4;
            this._CheckCompleteCtrlSpace.Tag = "sqlfwx::udiag.envset.ctrlspc";
            this._CheckCompleteCtrlSpace.Text = "   [C&trl]+[Space] toggles auto-complete";
            this._CheckCompleteCtrlSpace.UseVisualStyleBackColor = true;
            // 
            // _LabelLangDebug
            // 
            this._LabelLangDebug.AutoSize = true;
            this._LabelLangDebug.Location = new System.Drawing.Point(44, 172);
            this._LabelLangDebug.Name = "_LabelLangDebug";
            this._LabelLangDebug.Size = new System.Drawing.Size(166, 17);
            this._LabelLangDebug.TabIndex = 6;
            this._LabelLangDebug.Tag = "sqlfwx::udiag.envset.ldeb";
            this._LabelLangDebug.Text = "La&nguages and Debugging";
            // 
            // _LabelLangDebugIcon
            // 
            this._LabelLangDebugIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelLangDebugIcon.Image")));
            this._LabelLangDebugIcon.Location = new System.Drawing.Point(21, 171);
            this._LabelLangDebugIcon.Name = "_LabelLangDebugIcon";
            this._LabelLangDebugIcon.Size = new System.Drawing.Size(24, 21);
            this._LabelLangDebugIcon.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this._LabelLanguage);
            this.panel1.Controls.Add(this._CheckDebug);
            this.panel1.Controls.Add(this._LcboLanguage);
            this.panel1.Location = new System.Drawing.Point(24, 195);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(651, 131);
            this.panel1.TabIndex = 8;
            // 
            // _LabelLanguage
            // 
            this._LabelLanguage.AutoSize = true;
            this._LabelLanguage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLanguage.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLanguage.Location = new System.Drawing.Point(19, 20);
            this._LabelLanguage.Name = "_LabelLanguage";
            this._LabelLanguage.Size = new System.Drawing.Size(135, 13);
            this._LabelLanguage.TabIndex = 5;
            this._LabelLanguage.Tag = "ledx::settings.language.text";
            this._LabelLanguage.Text = "User Interface &Language:";
            // 
            // _CheckDebug
            // 
            this._CheckDebug.AutoSize = true;
            this._CheckDebug.Location = new System.Drawing.Point(22, 86);
            this._CheckDebug.Name = "_CheckDebug";
            this._CheckDebug.Size = new System.Drawing.Size(372, 21);
            this._CheckDebug.TabIndex = 4;
            this._CheckDebug.Tag = "debugx:::settings.showdebug";
            this._CheckDebug.Text = "   Enable &debug features (show Debug menu in menu strip)";
            this._CheckDebug.UseVisualStyleBackColor = true;
            // 
            // _LcboLanguage
            // 
            this._LcboLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LcboLanguage.Location = new System.Drawing.Point(22, 37);
            this._LcboLanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LcboLanguage.Name = "_LcboLanguage";
            this._LcboLanguage.Size = new System.Drawing.Size(597, 31);
            this._LcboLanguage.TabIndex = 0;
            this._LcboLanguage.Value = null;
            // 
            // EnvironmentSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelLangDebug);
            this.Controls.Add(this._LabelAutoComplete);
            this.Controls.Add(this._LabelLangDebugIcon);
            this.Controls.Add(this._LabelAutoCompleteIcon);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._PanelAutoComplete);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "EnvironmentSettingsControl";
            this.Size = new System.Drawing.Size(697, 349);
            this._PanelAutoComplete.ResumeLayout(false);
            this._PanelAutoComplete.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelAutoComplete;
        private System.Windows.Forms.Label _LabelAutoCompleteIcon;
        private System.Windows.Forms.Panel _PanelAutoComplete;
        private System.Windows.Forms.CheckBox _CheckCompleteCtrlSpace;
        private System.Windows.Forms.CheckBox _CheckAutoStart;
        private System.Windows.Forms.CheckBox _CheckCompleteSapce;
        private System.Windows.Forms.Label _LabelLangDebug;
        private System.Windows.Forms.Label _LabelLangDebugIcon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox _CheckDebug;
        private Robbiblubber.Util.Localization.Controls.LocaleComboBox _LcboLanguage;
        private System.Windows.Forms.Label _LabelLanguage;
    }
}
