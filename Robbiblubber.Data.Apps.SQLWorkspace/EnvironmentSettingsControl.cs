﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>This class implements the UI settings control.</summary>
    public partial class EnvironmentSettingsControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public EnvironmentSettingsControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
        /// <summary>Initializes the settings window.</summary>
        /// <param name="main">Main window.</param>
        public void InitSettings(FormMain main)
        {
            _CheckDebug.Checked = main.__ShowDebug;

            _CheckAutoStart.Checked = main._AutoAutoStart;
            _CheckCompleteCtrlSpace.Checked = main._AutoCtrlSpace;
            _CheckCompleteSapce.Checked = main._AutoApplySpace;
        }


        /// <summary>Applies the current settings.</summary>
        /// <param name="main">Main window.</param>
        public void ApplySettings(FormMain main)
        {
            main.__ShowDebug = _CheckDebug.Checked;

            main._AutoAutoStart = _CheckAutoStart.Checked;
            main._AutoCtrlSpace = _CheckCompleteCtrlSpace.Checked;
            main._AutoApplySpace = _CheckCompleteSapce.Checked;

            main.__SaveSettings();

            if(_LcboLanguage.Value != Locale.Selected)
            {
                Locale.Selected = _LcboLanguage.Value;
                Locale.SaveSelection("Robbiblubber.Util.NET");

                main.Localize();
            }
        }
    }
}
