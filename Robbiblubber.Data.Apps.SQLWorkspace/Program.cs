﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util;
using Robbiblubber.Data.Interpreters;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>SQL Workspace program</summary>
    internal class Program: IProgram
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Program()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProgram                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the program supports an explorer.</summary>
        public bool SupportsExplorer
        {
            get { return true; }
        }


        /// <summary>Initializes the program.</summary>
        public void Init()
        {
            PathOp.Initialize("robbiblubber.org/SQLWorkspace");
            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");

            Settings.AddPage("settings::general", "General", typeof(GeneralSettingsControl));
            Settings.AddPage("settings::env", "Environment", typeof(EnvironmentSettingsControl));
            Settings.AddPage("settings::editor", "Editor", typeof(TextEditorSettingsControl));
        }


        /// <summary>Shows a splash window.</summary>
        /// <returns>Splash window.</returns>
        public ISplashWindow ShowSplash()
        {
            FormSplash f = new FormSplash();
            f.Show();

            return f;
        }


        /// <summary>Shows an about window.</summary>
        /// <returns>Result.</returns>
        public DialogResult ShowAbout()
        {
            FormAbout f = new FormAbout();
            return f.ShowDialog();
        }
    }
}
