﻿using System;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>Splash screens implement this interface.</summary>
    public interface ISplashWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Waits some time before closing the window.</summary>
        void DelayClose();
    }
}
