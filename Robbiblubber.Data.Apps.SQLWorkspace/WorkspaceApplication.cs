﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

using Robbiblubber.Util;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>Program class.</summary>
    public static class WorkspaceApplication
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Main form.</summary>
        internal static FormMain _Main;

        /// <summary>Splash form.</summary>
        internal static ISplashWindow _Splash;

        /// <summary>Program class.</summary>
        internal static IProgram _Program = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>File name.</summary>
        public static string __FileName = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="args">Command line arguments.</param>
        [STAThread]
        static void Main(string[] args)
        {
            foreach(string i in args)
            {
                if(File.Exists(i)) { __FileName = i; break; }
            }
            
            Run(new Program());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Runs the program.</summary>
        /// <param name="program">Program.</param>
        public static void Run(IProgram program)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            _Program = program;

            _Program.Init();

            _Splash = _Program.ShowSplash();
            Application.DoEvents();
            Thread.Sleep(1600);

            _Main = new FormMain();
            _Splash.DelayClose();

            Application.Run(_Main);
        }
    }
}
