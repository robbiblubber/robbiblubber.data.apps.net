﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>This class provides a list of menu items for explorers.</summary>
    internal class __MenuItemList: MutableList<ToolStripItem>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public __MenuItemList(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the list has recently been changed.</summary>
        public bool Changed
        {
            get 
            {
                if(ItemsAdded.Count > 0) return true;
                if(ItemsRemoved.Count > 0) return true;
                return false;
            }
        }


        /// <summary>Gets the recently added items.</summary>
        public IList<ToolStripItem> ItemsAdded
        {
            get;
        } = new List<ToolStripItem>();


        /// <summary>Gets the recently removed items.</summary>
        public IList<ToolStripItem> ItemsRemoved
        {
            get;
        } = new List<ToolStripItem>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Acknowledges changes and clears the change lists.</summary>
        public void AcknowledgeChanges()
        {
            ItemsAdded.Clear();
            ItemsRemoved.Clear();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] MutableList<ToolStripMenuItem>                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets an element by its index.</summary>
        /// <param name="i">Index.</param>
        public override ToolStripItem this[int i]
        {
            get { return base[i]; }
            set
            {
                if(base[i] == value) return;

                ItemsRemoved.Add(base[i]);
                ItemsAdded.Add(value);                
                base[i] = value;
            }
        }
        

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public override void Add(ToolStripItem item)
        {
            ItemsAdded.Add(item);
            base.Add(item);
        }


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        public override void AddRange(IEnumerable<ToolStripItem> items)
        {
            ((List<ToolStripItem>) ItemsAdded).AddRange(items);
            base.AddRange(items);
        }


        /// <summary>Removes an item.</summary>
        /// <param name="item">Item.</param>
        public override void Remove(ToolStripItem item)
        {
            ItemsRemoved.Add(item);
            base.Remove(item);
        }


        /// <summary>Removes the item with the given index.</summary>
        /// <param name="i">Index.</param>
        public override void RemoveAt(int i)
        {
            ItemsRemoved.Add(this[i]);
            base.RemoveAt(i);
        }


        /// <summary>Clears the list.</summary>
        public override void Clear()
        {
            ((List<ToolStripItem>) ItemsRemoved).AddRange(this);
            base.Clear();
        }
    }
}
