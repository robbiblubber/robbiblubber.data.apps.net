﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>This class implements the UI settings control.</summary>
    public partial class TextEditorSettingsControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TextEditorSettingsControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
        /// <summary>Initializes the settings window.</summary>
        /// <param name="main">Main window.</param>
        public void InitSettings(FormMain main)
        {
            _LabelInputText.BackColor = _PanelInputText.BackColor = main.__RtfIn.BackColor;
            _LabelInputText.ForeColor = main.__RtfIn.ForeColor;
            _LabelInputText.Font = main.__RtfIn.Font;

            _LabelOutputText.BackColor = _PanelOutputText.BackColor = main.__RtfOut.BackColor;
            _LabelOutputText.ForeColor = main.__RtfOut.ForeColor;
            _LabelOutputText.Font = main.__RtfOut.Font;

            _CheckExecuteAltEnter.Checked = main._ExecuteOnAltEnter;
            _CheckExecuteCtrlEnter.Checked = main._ExecuteOnCtrlEnter;
        }


        /// <summary>Applies the current settings.</summary>
        /// <param name="main">Main window.</param>
        public void ApplySettings(FormMain main)
        {
            main.__RtfIn.BackColor = _PanelInputText.BackColor;
            main.__RtfIn.ForeColor = _LabelInputText.ForeColor;
            main.__RtfIn.Font = _LabelInputText.Font;

            main.__RtfOut.BackColor = _PanelOutputText.BackColor;
            main.__RtfOut.ForeColor = _LabelOutputText.ForeColor;
            main.__RtfOut.Font = _LabelOutputText.Font;

            main._ExecuteOnAltEnter = _CheckExecuteAltEnter.Checked;
            main._ExecuteOnCtrlEnter = _CheckExecuteCtrlEnter.Checked;

            main.__SaveLookAndFeel();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Link "Input font" click.</summary>
        private void _LinkInputFont_Click(object sender, EventArgs e)
        {
            FontDialog f = new FontDialog();
            f.Font = _LabelInputText.Font;
            f.Color = _LabelInputText.ForeColor;
            f.ShowColor = true;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _LabelInputText.Font = f.Font;
                _LabelInputText.ForeColor = f.Color;
            }
        }


        /// <summary>Link "Output font" click.</summary>
        private void _LinkOutputFont_Click(object sender, EventArgs e)
        {
            FontDialog f = new FontDialog();
            f.Font = _LabelOutputText.Font;
            f.Color = _LabelOutputText.ForeColor;
            f.ShowColor = true;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _LabelOutputText.Font = f.Font;
                _LabelOutputText.ForeColor = f.Color;
            }
        }


        /// <summary>Link "Input back color" click.</summary>
        private void _LinkInputBgColor_Click(object sender, EventArgs e)
        {
            ColorDialog f = new ColorDialog();
            f.Color = _PanelInputText.BackColor;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _PanelInputText.BackColor = _LabelInputText.BackColor = f.Color;
            }
        }


        /// <summary>Link "Output back color" click.</summary>
        private void _LinkOutputBgColor_Click(object sender, EventArgs e)
        {
            ColorDialog f = new ColorDialog();
            f.Color = _PanelOutputText.BackColor;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _PanelOutputText.BackColor = _LabelOutputText.BackColor = f.Color;
            }
        }

        
        /// <summary>Link "Input text color" click.</summary>
        private void _LinkInputTextColor_Click(object sender, EventArgs e)
        {
            ColorDialog f = new ColorDialog();
            f.Color = _LabelInputText.ForeColor;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _LabelInputText.ForeColor = f.Color;
            }
        }


        /// <summary>Link "Output text color" click.</summary>
        private void _LinkOutputTextColor_Click(object sender, EventArgs e)
        {
            ColorDialog f = new ColorDialog();
            f.Color = _LabelOutputText.ForeColor;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _LabelOutputText.ForeColor = f.Color;
            }
        }
    }
}
