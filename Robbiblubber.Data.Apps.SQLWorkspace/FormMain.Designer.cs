﻿namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this._MenuMain = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDatabases = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuConnect = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuRecent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRedo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuFind = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuReplace = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGoTo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditBlank2 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDeselectAll = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditBlank3 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuClear = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowExplorer = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuViewBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuText = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTable = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDatabase = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRefersh = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuConsole = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExecute = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExecuteCurrent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExecuteToCurrent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEexecuteFromCurrent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbort = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuConsoleBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCommit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRollback = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBrowseHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFavoriteSQL = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHistoryBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDumpHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHistoryBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCleanUpHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExplorer = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExplorerFind = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExplorerFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExport = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuToolsBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._SplitMain = new System.Windows.Forms.SplitContainer();
            this._TreeStructure = new System.Windows.Forms.TreeView();
            this._CmenuTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this._IlistTree = new System.Windows.Forms.ImageList(this.components);
            this._SplitSQL = new System.Windows.Forms.SplitContainer();
            this.@__RtfIn = new System.Windows.Forms.RichTextBox();
            this._CmenuIn = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuInCut = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuInCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuInPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._CmneuInBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuInExecute = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuInBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuInCommit = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuInRollback = new System.Windows.Forms.ToolStripMenuItem();
            this.@__GridOut = new System.Windows.Forms.DataGridView();
            this._CmenuOut = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuOutCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.@__RtfOut = new System.Windows.Forms.RichTextBox();
            this._WaitMain = new Robbiblubber.Util.Controls.WaitControl();
            this._GotoSearch = new Robbiblubber.Util.Controls.GoToControl();
            this._ReplaceSearch = new Robbiblubber.Util.Controls.ReplaceControl();
            this._FindSearch = new Robbiblubber.Util.Controls.FindControl();
            this._CmenuDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuTreeBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._SplitMain)).BeginInit();
            this._SplitMain.Panel1.SuspendLayout();
            this._SplitMain.Panel2.SuspendLayout();
            this._SplitMain.SuspendLayout();
            this._CmenuTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._SplitSQL)).BeginInit();
            this._SplitSQL.Panel1.SuspendLayout();
            this._SplitSQL.Panel2.SuspendLayout();
            this._SplitSQL.SuspendLayout();
            this._CmenuIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.@__GridOut)).BeginInit();
            this._CmenuOut.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuMain
            // 
            this._MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuView,
            this._MenuDatabase,
            this._MenuConsole,
            this._MenuTools,
            this._MenuHelp});
            this._MenuMain.Location = new System.Drawing.Point(0, 0);
            this._MenuMain.Name = "_MenuMain";
            this._MenuMain.Size = new System.Drawing.Size(1041, 24);
            this._MenuMain.TabIndex = 0;
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDatabases,
            this._MenuConnect,
            this._MenuDisconnect,
            this._MenuFileBlank0,
            this._MenuOpen,
            this._MenuSave,
            this._MenuSaveAs,
            this._MenuFileBlank1,
            this._MenuRecent,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 20);
            this._MenuFile.Tag = "sqlfwx::menu.file";
            this._MenuFile.Text = "&File";
            this._MenuFile.DropDownClosed += new System.EventHandler(this._MenuFile_DropDownClosed);
            this._MenuFile.DropDownOpening += new System.EventHandler(this._MenuFile_DropDownOpening);
            // 
            // _MenuDatabases
            // 
            this._MenuDatabases.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDatabases.Image")));
            this._MenuDatabases.Name = "_MenuDatabases";
            this._MenuDatabases.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this._MenuDatabases.Size = new System.Drawing.Size(195, 22);
            this._MenuDatabases.Tag = "sqlfwx::menu.databases";
            this._MenuDatabases.Text = "&Databases...";
            this._MenuDatabases.Click += new System.EventHandler(this._MenuDatabases_Click);
            // 
            // _MenuConnect
            // 
            this._MenuConnect.Image = ((System.Drawing.Image)(resources.GetObject("_MenuConnect.Image")));
            this._MenuConnect.Name = "_MenuConnect";
            this._MenuConnect.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F12)));
            this._MenuConnect.Size = new System.Drawing.Size(195, 22);
            this._MenuConnect.Tag = "sqlfwx::menu.connect";
            this._MenuConnect.Text = "&Connect...";
            this._MenuConnect.Click += new System.EventHandler(this._MenuConnect_Click);
            // 
            // _MenuDisconnect
            // 
            this._MenuDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDisconnect.Image")));
            this._MenuDisconnect.Name = "_MenuDisconnect";
            this._MenuDisconnect.Size = new System.Drawing.Size(195, 22);
            this._MenuDisconnect.Tag = "sqlfwx::menu.disconnect";
            this._MenuDisconnect.Text = "&Disconnect";
            this._MenuDisconnect.Click += new System.EventHandler(this._MenuDisconnect_Click);
            // 
            // _MenuFileBlank0
            // 
            this._MenuFileBlank0.Name = "_MenuFileBlank0";
            this._MenuFileBlank0.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuOpen
            // 
            this._MenuOpen.Image = ((System.Drawing.Image)(resources.GetObject("_MenuOpen.Image")));
            this._MenuOpen.Name = "_MenuOpen";
            this._MenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._MenuOpen.Size = new System.Drawing.Size(195, 22);
            this._MenuOpen.Tag = "sqlfwx::menu.open";
            this._MenuOpen.Text = "&Open...";
            this._MenuOpen.Click += new System.EventHandler(this._MenuOpen_Click);
            // 
            // _MenuSave
            // 
            this._MenuSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSave.Image")));
            this._MenuSave.Name = "_MenuSave";
            this._MenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSave.Size = new System.Drawing.Size(195, 22);
            this._MenuSave.Tag = "sqlfwx::menu.save";
            this._MenuSave.Text = "&Save";
            this._MenuSave.Click += new System.EventHandler(this._MenuSave_Click);
            // 
            // _MenuSaveAs
            // 
            this._MenuSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSaveAs.Image")));
            this._MenuSaveAs.Name = "_MenuSaveAs";
            this._MenuSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this._MenuSaveAs.Size = new System.Drawing.Size(195, 22);
            this._MenuSaveAs.Tag = "sqlfwx::menu.saveas";
            this._MenuSaveAs.Text = "Save &As...";
            this._MenuSaveAs.Click += new System.EventHandler(this._MenuSaveAs_Click);
            // 
            // _MenuFileBlank1
            // 
            this._MenuFileBlank1.Name = "_MenuFileBlank1";
            this._MenuFileBlank1.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuRecent
            // 
            this._MenuRecent.Name = "_MenuRecent";
            this._MenuRecent.Size = new System.Drawing.Size(195, 22);
            this._MenuRecent.Tag = "sqlfwx::menu.recent";
            this._MenuRecent.Text = "&Recent Files";
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.Size = new System.Drawing.Size(195, 22);
            this._MenuExit.Tag = "sqlfwx::menu.exit";
            this._MenuExit.Text = "E&xit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuUndo,
            this._MenuRedo,
            this._MenuEditBlank0,
            this._MenuCut,
            this._MenuCopy,
            this._MenuPaste,
            this._MenuEditBlank1,
            this._MenuFind,
            this._MenuFindNext,
            this._MenuReplace,
            this._MenuGoTo,
            this._MenuEditBlank2,
            this._MenuSelectAll,
            this._MenuDeselectAll,
            this._MenuEditBlank3,
            this._MenuClear});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 20);
            this._MenuEdit.Tag = "sqlfwx::menu.edit";
            this._MenuEdit.Text = "&Edit";
            this._MenuEdit.DropDownOpening += new System.EventHandler(this._MenuEdit_DropDownOpening);
            // 
            // _MenuUndo
            // 
            this._MenuUndo.Image = ((System.Drawing.Image)(resources.GetObject("_MenuUndo.Image")));
            this._MenuUndo.Name = "_MenuUndo";
            this._MenuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._MenuUndo.Size = new System.Drawing.Size(180, 22);
            this._MenuUndo.Tag = "sqlfwx::menu.undo";
            this._MenuUndo.Text = "&Undo";
            this._MenuUndo.Click += new System.EventHandler(this._MenuUndo_Click);
            // 
            // _MenuRedo
            // 
            this._MenuRedo.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRedo.Image")));
            this._MenuRedo.Name = "_MenuRedo";
            this._MenuRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this._MenuRedo.Size = new System.Drawing.Size(180, 22);
            this._MenuRedo.Tag = "sqlfwx::menu.redo";
            this._MenuRedo.Text = "Red&o";
            // 
            // _MenuEditBlank0
            // 
            this._MenuEditBlank0.Name = "_MenuEditBlank0";
            this._MenuEditBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuCut
            // 
            this._MenuCut.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCut.Image")));
            this._MenuCut.Name = "_MenuCut";
            this._MenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._MenuCut.Size = new System.Drawing.Size(180, 22);
            this._MenuCut.Tag = "sqlfwx::menu.cut";
            this._MenuCut.Text = "C&ut";
            this._MenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _MenuCopy
            // 
            this._MenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCopy.Image")));
            this._MenuCopy.Name = "_MenuCopy";
            this._MenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuCopy.Size = new System.Drawing.Size(180, 22);
            this._MenuCopy.Tag = "sqlfwx::menu.copy";
            this._MenuCopy.Text = "&Copy";
            this._MenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _MenuPaste
            // 
            this._MenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPaste.Image")));
            this._MenuPaste.Name = "_MenuPaste";
            this._MenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._MenuPaste.Size = new System.Drawing.Size(180, 22);
            this._MenuPaste.Tag = "sqlfwx::menu.paste";
            this._MenuPaste.Text = "&Paste";
            this._MenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _MenuEditBlank1
            // 
            this._MenuEditBlank1.Name = "_MenuEditBlank1";
            this._MenuEditBlank1.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuFind
            // 
            this._MenuFind.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFind.Image")));
            this._MenuFind.Name = "_MenuFind";
            this._MenuFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._MenuFind.Size = new System.Drawing.Size(180, 22);
            this._MenuFind.Tag = "sqlfwx::menu.find";
            this._MenuFind.Text = "&Find...";
            this._MenuFind.Click += new System.EventHandler(this._MenuFind_Click);
            // 
            // _MenuFindNext
            // 
            this._MenuFindNext.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFindNext.Image")));
            this._MenuFindNext.Name = "_MenuFindNext";
            this._MenuFindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this._MenuFindNext.Size = new System.Drawing.Size(180, 22);
            this._MenuFindNext.Tag = "sqlfwx::menu.findnext";
            this._MenuFindNext.Text = "Find &Next";
            this._MenuFindNext.Click += new System.EventHandler(this._MenuFindNext_Click);
            // 
            // _MenuReplace
            // 
            this._MenuReplace.Image = ((System.Drawing.Image)(resources.GetObject("_MenuReplace.Image")));
            this._MenuReplace.Name = "_MenuReplace";
            this._MenuReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this._MenuReplace.Size = new System.Drawing.Size(180, 22);
            this._MenuReplace.Tag = "sqlfwx::menu.replace";
            this._MenuReplace.Text = "&Replace...";
            this._MenuReplace.Click += new System.EventHandler(this._MenuReplace_Click);
            // 
            // _MenuGoTo
            // 
            this._MenuGoTo.Name = "_MenuGoTo";
            this._MenuGoTo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this._MenuGoTo.Size = new System.Drawing.Size(180, 22);
            this._MenuGoTo.Tag = "sqlfwx::menu.goto";
            this._MenuGoTo.Text = "&Go To...";
            this._MenuGoTo.Click += new System.EventHandler(this._MenuGoTo_Click);
            // 
            // _MenuEditBlank2
            // 
            this._MenuEditBlank2.Name = "_MenuEditBlank2";
            this._MenuEditBlank2.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuSelectAll
            // 
            this._MenuSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSelectAll.Image")));
            this._MenuSelectAll.Name = "_MenuSelectAll";
            this._MenuSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this._MenuSelectAll.Size = new System.Drawing.Size(180, 22);
            this._MenuSelectAll.Tag = "sqlfwx::menu.selectall";
            this._MenuSelectAll.Text = "Select &All";
            this._MenuSelectAll.Click += new System.EventHandler(this._MenuSelectAll_Click);
            // 
            // _MenuDeselectAll
            // 
            this._MenuDeselectAll.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDeselectAll.Image")));
            this._MenuDeselectAll.Name = "_MenuDeselectAll";
            this._MenuDeselectAll.Size = new System.Drawing.Size(180, 22);
            this._MenuDeselectAll.Tag = "sqlfwx::menu.deselectall";
            this._MenuDeselectAll.Text = "&Deselect All";
            this._MenuDeselectAll.Click += new System.EventHandler(this._MenuDeselectAll_Click);
            // 
            // _MenuEditBlank3
            // 
            this._MenuEditBlank3.Name = "_MenuEditBlank3";
            this._MenuEditBlank3.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuClear
            // 
            this._MenuClear.Image = ((System.Drawing.Image)(resources.GetObject("_MenuClear.Image")));
            this._MenuClear.Name = "_MenuClear";
            this._MenuClear.Size = new System.Drawing.Size(180, 22);
            this._MenuClear.Tag = "sqlfwx::menu.clear";
            this._MenuClear.Text = "C&lear";
            this._MenuClear.Click += new System.EventHandler(this._MenuClear_Click);
            // 
            // _MenuView
            // 
            this._MenuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowExplorer,
            this._MenuViewBlank0,
            this._MenuText,
            this._MenuTable});
            this._MenuView.Name = "_MenuView";
            this._MenuView.Size = new System.Drawing.Size(44, 20);
            this._MenuView.Tag = "sqlfwx::menu.view";
            this._MenuView.Text = "&View";
            // 
            // _MenuShowExplorer
            // 
            this._MenuShowExplorer.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowExplorer.Image")));
            this._MenuShowExplorer.Name = "_MenuShowExplorer";
            this._MenuShowExplorer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this._MenuShowExplorer.Size = new System.Drawing.Size(180, 22);
            this._MenuShowExplorer.Tag = "sqlfwx::menu.explorer";
            this._MenuShowExplorer.Text = "&Explorer";
            this._MenuShowExplorer.Click += new System.EventHandler(this._MenuShowExplorer_Click);
            // 
            // _MenuViewBlank0
            // 
            this._MenuViewBlank0.Name = "_MenuViewBlank0";
            this._MenuViewBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuText
            // 
            this._MenuText.Checked = true;
            this._MenuText.CheckState = System.Windows.Forms.CheckState.Checked;
            this._MenuText.Image = ((System.Drawing.Image)(resources.GetObject("_MenuText.Image")));
            this._MenuText.Name = "_MenuText";
            this._MenuText.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this._MenuText.Size = new System.Drawing.Size(180, 22);
            this._MenuText.Tag = "sqlfwx::menu.text";
            this._MenuText.Text = "Te&xt";
            this._MenuText.Click += new System.EventHandler(this._MenuText_Click);
            // 
            // _MenuTable
            // 
            this._MenuTable.Image = ((System.Drawing.Image)(resources.GetObject("_MenuTable.Image")));
            this._MenuTable.Name = "_MenuTable";
            this._MenuTable.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this._MenuTable.Size = new System.Drawing.Size(180, 22);
            this._MenuTable.Tag = "sqlfwx::menu.table";
            this._MenuTable.Text = "&Table";
            this._MenuTable.Click += new System.EventHandler(this._MenuTable_Click);
            // 
            // _MenuDatabase
            // 
            this._MenuDatabase.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuRefersh});
            this._MenuDatabase.Name = "_MenuDatabase";
            this._MenuDatabase.Size = new System.Drawing.Size(67, 20);
            this._MenuDatabase.Text = "&Database";
            this._MenuDatabase.DropDownOpening += new System.EventHandler(this._MenuDatabase_DropDownOpening);
            // 
            // _MenuRefersh
            // 
            this._MenuRefersh.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRefersh.Image")));
            this._MenuRefersh.Name = "_MenuRefersh";
            this._MenuRefersh.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this._MenuRefersh.Size = new System.Drawing.Size(180, 22);
            this._MenuRefersh.Tag = "sqlfwx::menu.refresh";
            this._MenuRefersh.Text = "&Refresh";
            // 
            // _MenuConsole
            // 
            this._MenuConsole.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuExecute,
            this._MenuExecuteCurrent,
            this._MenuExecuteToCurrent,
            this._MenuEexecuteFromCurrent,
            this._MenuAbort,
            this._MenuConsoleBlank0,
            this._MenuCommit,
            this._MenuRollback});
            this._MenuConsole.Name = "_MenuConsole";
            this._MenuConsole.Size = new System.Drawing.Size(62, 20);
            this._MenuConsole.Tag = "sqlfwx::menu.console";
            this._MenuConsole.Text = "&Console";
            this._MenuConsole.DropDownClosed += new System.EventHandler(this._MenuConsole_DropDownClosed);
            this._MenuConsole.DropDownOpening += new System.EventHandler(this._MenuConsole_DropDownOpening);
            // 
            // _MenuExecute
            // 
            this._MenuExecute.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExecute.Image")));
            this._MenuExecute.Name = "_MenuExecute";
            this._MenuExecute.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._MenuExecute.Size = new System.Drawing.Size(258, 22);
            this._MenuExecute.Tag = "sqlfwx::menu.execute";
            this._MenuExecute.Text = "&Execute";
            this._MenuExecute.Click += new System.EventHandler(this._MenuExecute_Click);
            // 
            // _MenuExecuteCurrent
            // 
            this._MenuExecuteCurrent.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExecuteCurrent.Image")));
            this._MenuExecuteCurrent.Name = "_MenuExecuteCurrent";
            this._MenuExecuteCurrent.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this._MenuExecuteCurrent.Size = new System.Drawing.Size(258, 22);
            this._MenuExecuteCurrent.Tag = "sqlfwx::menu.xcur";
            this._MenuExecuteCurrent.Text = "E&xecute Current";
            this._MenuExecuteCurrent.Click += new System.EventHandler(this._MenuExecuteCurrent_Click);
            // 
            // _MenuExecuteToCurrent
            // 
            this._MenuExecuteToCurrent.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExecuteToCurrent.Image")));
            this._MenuExecuteToCurrent.Name = "_MenuExecuteToCurrent";
            this._MenuExecuteToCurrent.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F5)));
            this._MenuExecuteToCurrent.Size = new System.Drawing.Size(258, 22);
            this._MenuExecuteToCurrent.Tag = "sqlfwx::menu.xtocur";
            this._MenuExecuteToCurrent.Text = "Execute &To Current";
            this._MenuExecuteToCurrent.Click += new System.EventHandler(this._MenuExecuteToCurrent_Click);
            // 
            // _MenuEexecuteFromCurrent
            // 
            this._MenuEexecuteFromCurrent.Image = ((System.Drawing.Image)(resources.GetObject("_MenuEexecuteFromCurrent.Image")));
            this._MenuEexecuteFromCurrent.Name = "_MenuEexecuteFromCurrent";
            this._MenuEexecuteFromCurrent.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.F5)));
            this._MenuEexecuteFromCurrent.Size = new System.Drawing.Size(258, 22);
            this._MenuEexecuteFromCurrent.Tag = "sqlfwx::menu.xfromcur";
            this._MenuEexecuteFromCurrent.Text = "Execute &From Current";
            this._MenuEexecuteFromCurrent.Click += new System.EventHandler(this._MenuEexecuteFromCurrent_Click);
            // 
            // _MenuAbort
            // 
            this._MenuAbort.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAbort.Image")));
            this._MenuAbort.Name = "_MenuAbort";
            this._MenuAbort.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuAbort.Size = new System.Drawing.Size(258, 22);
            this._MenuAbort.Tag = "sqlfwx::menu.abort";
            this._MenuAbort.Text = "&Abort";
            this._MenuAbort.Visible = false;
            this._MenuAbort.Click += new System.EventHandler(this._MenuAbort_Click);
            // 
            // _MenuConsoleBlank0
            // 
            this._MenuConsoleBlank0.Name = "_MenuConsoleBlank0";
            this._MenuConsoleBlank0.Size = new System.Drawing.Size(255, 6);
            // 
            // _MenuCommit
            // 
            this._MenuCommit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCommit.Image")));
            this._MenuCommit.Name = "_MenuCommit";
            this._MenuCommit.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.C)));
            this._MenuCommit.Size = new System.Drawing.Size(258, 22);
            this._MenuCommit.Tag = "sqlfwx::menu.commit";
            this._MenuCommit.Text = "&Commit";
            this._MenuCommit.Click += new System.EventHandler(this._MenuCommit_Click);
            // 
            // _MenuRollback
            // 
            this._MenuRollback.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRollback.Image")));
            this._MenuRollback.Name = "_MenuRollback";
            this._MenuRollback.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
            this._MenuRollback.Size = new System.Drawing.Size(258, 22);
            this._MenuRollback.Tag = "sqlfwx::menu.rollback";
            this._MenuRollback.Text = "&Rollback";
            this._MenuRollback.Click += new System.EventHandler(this._MenuRollback_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuHistory,
            this._MenuExplorer,
            this._MenuExport,
            this._MenuToolsBlank0,
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(46, 20);
            this._MenuTools.Tag = "sqlfwx::menu.tools";
            this._MenuTools.Text = "&Tools";
            this._MenuTools.DropDownClosed += new System.EventHandler(this._MenuTools_DropDownClosed);
            this._MenuTools.DropDownOpening += new System.EventHandler(this._MenuTools_DropDownOpening);
            // 
            // _MenuHistory
            // 
            this._MenuHistory.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuBrowseHistory,
            this._MenuFavoriteSQL,
            this._MenuHistoryBlank0,
            this._MenuDumpHistory,
            this._MenuHistoryBlank1,
            this._MenuCleanUpHistory});
            this._MenuHistory.Name = "_MenuHistory";
            this._MenuHistory.Size = new System.Drawing.Size(180, 22);
            this._MenuHistory.Tag = "sqlfwx::menu.history";
            this._MenuHistory.Text = "&History";
            // 
            // _MenuBrowseHistory
            // 
            this._MenuBrowseHistory.Image = ((System.Drawing.Image)(resources.GetObject("_MenuBrowseHistory.Image")));
            this._MenuBrowseHistory.Name = "_MenuBrowseHistory";
            this._MenuBrowseHistory.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this._MenuBrowseHistory.Size = new System.Drawing.Size(192, 22);
            this._MenuBrowseHistory.Tag = "sqlfwx::menu.browsehistory";
            this._MenuBrowseHistory.Text = "&Browse...";
            this._MenuBrowseHistory.Click += new System.EventHandler(this._MenuBrowseHistory_Click);
            // 
            // _MenuFavoriteSQL
            // 
            this._MenuFavoriteSQL.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFavoriteSQL.Image")));
            this._MenuFavoriteSQL.Name = "_MenuFavoriteSQL";
            this._MenuFavoriteSQL.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this._MenuFavoriteSQL.Size = new System.Drawing.Size(192, 22);
            this._MenuFavoriteSQL.Tag = "sqlfwx::menu.favoritesql";
            this._MenuFavoriteSQL.Text = "&Favorite SQL...";
            this._MenuFavoriteSQL.Click += new System.EventHandler(this._MenuFavoriteSQL_Click);
            // 
            // _MenuHistoryBlank0
            // 
            this._MenuHistoryBlank0.Name = "_MenuHistoryBlank0";
            this._MenuHistoryBlank0.Size = new System.Drawing.Size(189, 6);
            // 
            // _MenuDumpHistory
            // 
            this._MenuDumpHistory.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDumpHistory.Image")));
            this._MenuDumpHistory.Name = "_MenuDumpHistory";
            this._MenuDumpHistory.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this._MenuDumpHistory.Size = new System.Drawing.Size(192, 22);
            this._MenuDumpHistory.Tag = "sqlfwx::menu.dumphistory";
            this._MenuDumpHistory.Text = "&Dump...";
            this._MenuDumpHistory.Click += new System.EventHandler(this._MenuDumpHistory_Click);
            // 
            // _MenuHistoryBlank1
            // 
            this._MenuHistoryBlank1.Name = "_MenuHistoryBlank1";
            this._MenuHistoryBlank1.Size = new System.Drawing.Size(189, 6);
            // 
            // _MenuCleanUpHistory
            // 
            this._MenuCleanUpHistory.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCleanUpHistory.Image")));
            this._MenuCleanUpHistory.Name = "_MenuCleanUpHistory";
            this._MenuCleanUpHistory.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this._MenuCleanUpHistory.Size = new System.Drawing.Size(192, 22);
            this._MenuCleanUpHistory.Tag = "sqlfwx::menu.cleanup";
            this._MenuCleanUpHistory.Text = "&Clean up...";
            this._MenuCleanUpHistory.Click += new System.EventHandler(this._MenuCleanUpHistory_Click);
            // 
            // _MenuExplorer
            // 
            this._MenuExplorer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuExplorerFind,
            this._MenuExplorerFindNext});
            this._MenuExplorer.Name = "_MenuExplorer";
            this._MenuExplorer.Size = new System.Drawing.Size(180, 22);
            this._MenuExplorer.Tag = "sqlfwx::menu.explorer";
            this._MenuExplorer.Text = "&Explorer";
            // 
            // _MenuExplorerFind
            // 
            this._MenuExplorerFind.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExplorerFind.Image")));
            this._MenuExplorerFind.Name = "_MenuExplorerFind";
            this._MenuExplorerFind.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this._MenuExplorerFind.Size = new System.Drawing.Size(178, 22);
            this._MenuExplorerFind.Tag = "sqlfwx::menu.find";
            this._MenuExplorerFind.Text = "&Find...";
            this._MenuExplorerFind.Click += new System.EventHandler(this._MenuExplorerFind_Click);
            // 
            // _MenuExplorerFindNext
            // 
            this._MenuExplorerFindNext.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExplorerFindNext.Image")));
            this._MenuExplorerFindNext.Name = "_MenuExplorerFindNext";
            this._MenuExplorerFindNext.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this._MenuExplorerFindNext.Size = new System.Drawing.Size(178, 22);
            this._MenuExplorerFindNext.Tag = "sqlfwx::menu.findnext";
            this._MenuExplorerFindNext.Text = "Find &Next";
            this._MenuExplorerFindNext.Click += new System.EventHandler(this._MenuExplorerFindNext_Click);
            // 
            // _MenuExport
            // 
            this._MenuExport.Name = "_MenuExport";
            this._MenuExport.Size = new System.Drawing.Size(180, 22);
            this._MenuExport.Tag = "sqlfwx::menu.export";
            this._MenuExport.Text = "E&xport To";
            // 
            // _MenuToolsBlank0
            // 
            this._MenuToolsBlank0.Name = "_MenuToolsBlank0";
            this._MenuToolsBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(180, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "&Debug";
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStart.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "&Start Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStop.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "&Stop Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(219, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuDebugSave.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "Save &Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this._MenuDebugView.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.U)));
            this._MenuDebugUpload.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(180, 22);
            this._MenuSettings.Tag = "sqlfwx::menu.settings";
            this._MenuSettings.Text = "Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 20);
            this._MenuHelp.Tag = "sqlfwx::menu.help";
            this._MenuHelp.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._MenuShowHelp.Size = new System.Drawing.Size(150, 22);
            this._MenuShowHelp.Tag = "sqlfwx::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help";
            this._MenuShowHelp.Click += new System.EventHandler(this._MenuShowHelp_Click);
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(150, 22);
            this._MenuAbout.Tag = "sqlfwx::menu.about";
            this._MenuAbout.Text = "&About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _SplitMain
            // 
            this._SplitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SplitMain.Location = new System.Drawing.Point(0, 24);
            this._SplitMain.Name = "_SplitMain";
            // 
            // _SplitMain.Panel1
            // 
            this._SplitMain.Panel1.Controls.Add(this._TreeStructure);
            // 
            // _SplitMain.Panel2
            // 
            this._SplitMain.Panel2.Controls.Add(this._SplitSQL);
            this._SplitMain.Size = new System.Drawing.Size(1041, 539);
            this._SplitMain.SplitterDistance = 347;
            this._SplitMain.TabIndex = 1;
            // 
            // _TreeStructure
            // 
            this._TreeStructure.ContextMenuStrip = this._CmenuTree;
            this._TreeStructure.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TreeStructure.ImageIndex = 0;
            this._TreeStructure.ImageList = this._IlistTree;
            this._TreeStructure.Location = new System.Drawing.Point(0, 0);
            this._TreeStructure.Name = "_TreeStructure";
            this._TreeStructure.SelectedImageIndex = 0;
            this._TreeStructure.Size = new System.Drawing.Size(347, 539);
            this._TreeStructure.TabIndex = 2;
            this._TreeStructure.TabStop = false;
            this._TreeStructure.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this._TreeStructure_BeforeExpand);
            this._TreeStructure.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._TreeStructure_AfterSelect);
            this._TreeStructure.DoubleClick += new System.EventHandler(this._TreeStructure_DoubleClick);
            this._TreeStructure.KeyDown += new System.Windows.Forms.KeyEventHandler(this._TreeStructure_KeyDown);
            this._TreeStructure.KeyUp += new System.Windows.Forms.KeyEventHandler(this._TreeStructure_KeyUp);
            this._TreeStructure.MouseDown += new System.Windows.Forms.MouseEventHandler(this._TreeStructure_MouseDown);
            // 
            // _CmenuTree
            // 
            this._CmenuTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuDisconnect,
            this._CmenuTreeBlank0,
            this._CmenuRefresh});
            this._CmenuTree.Name = "_CMenuTree";
            this._CmenuTree.Size = new System.Drawing.Size(181, 76);
            this._CmenuTree.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this._CmenuTree_Closed);
            this._CmenuTree.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this._CmenuTree_Closing);
            this._CmenuTree.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuTree_Opening);
            this._CmenuTree.Opened += new System.EventHandler(this._CmenuTree_Opened);
            // 
            // _CmenuRefresh
            // 
            this._CmenuRefresh.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuRefresh.Image")));
            this._CmenuRefresh.Name = "_CmenuRefresh";
            this._CmenuRefresh.Size = new System.Drawing.Size(133, 22);
            this._CmenuRefresh.Tag = "sqlfwx::menu.refresh";
            this._CmenuRefresh.Text = "Refresh";
            this._CmenuRefresh.Click += new System.EventHandler(this._CmenuRefresh_Click);
            // 
            // _IlistTree
            // 
            this._IlistTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistTree.ImageStream")));
            this._IlistTree.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistTree.Images.SetKeyName(0, "ico::hourglass");
            // 
            // _SplitSQL
            // 
            this._SplitSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SplitSQL.Location = new System.Drawing.Point(0, 0);
            this._SplitSQL.Name = "_SplitSQL";
            this._SplitSQL.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // _SplitSQL.Panel1
            // 
            this._SplitSQL.Panel1.Controls.Add(this.@__RtfIn);
            // 
            // _SplitSQL.Panel2
            // 
            this._SplitSQL.Panel2.Controls.Add(this.@__GridOut);
            this._SplitSQL.Panel2.Controls.Add(this.@__RtfOut);
            this._SplitSQL.Panel2.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SplitSQL.Size = new System.Drawing.Size(690, 539);
            this._SplitSQL.SplitterDistance = 321;
            this._SplitSQL.TabIndex = 0;
            // 
            // __RtfIn
            // 
            this.@__RtfIn.AcceptsTab = true;
            this.@__RtfIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.@__RtfIn.ContextMenuStrip = this._CmenuIn;
            this.@__RtfIn.DetectUrls = false;
            this.@__RtfIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.@__RtfIn.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.@__RtfIn.Location = new System.Drawing.Point(0, 0);
            this.@__RtfIn.Name = "__RtfIn";
            this.@__RtfIn.ShortcutsEnabled = false;
            this.@__RtfIn.Size = new System.Drawing.Size(690, 321);
            this.@__RtfIn.TabIndex = 0;
            this.@__RtfIn.Text = "";
            this.@__RtfIn.WordWrap = false;
            this.@__RtfIn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.@__RtfIn_KeyDown);
            // 
            // _CmenuIn
            // 
            this._CmenuIn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuInCut,
            this._CmenuInCopy,
            this._CmenuInPaste,
            this._CmneuInBlank0,
            this._CmenuInExecute,
            this._CmenuInBlank1,
            this._CmenuInCommit,
            this._CmenuInRollback});
            this._CmenuIn.Name = "_CmenuIn";
            this._CmenuIn.Size = new System.Drawing.Size(120, 148);
            this._CmenuIn.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuIn_Opening);
            // 
            // _CmenuInCut
            // 
            this._CmenuInCut.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuInCut.Image")));
            this._CmenuInCut.Name = "_CmenuInCut";
            this._CmenuInCut.Size = new System.Drawing.Size(119, 22);
            this._CmenuInCut.Text = "Cut";
            this._CmenuInCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _CmenuInCopy
            // 
            this._CmenuInCopy.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuInCopy.Image")));
            this._CmenuInCopy.Name = "_CmenuInCopy";
            this._CmenuInCopy.Size = new System.Drawing.Size(119, 22);
            this._CmenuInCopy.Text = "Copy";
            this._CmenuInCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _CmenuInPaste
            // 
            this._CmenuInPaste.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuInPaste.Image")));
            this._CmenuInPaste.Name = "_CmenuInPaste";
            this._CmenuInPaste.Size = new System.Drawing.Size(119, 22);
            this._CmenuInPaste.Text = "Paste";
            this._CmenuInPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _CmneuInBlank0
            // 
            this._CmneuInBlank0.Name = "_CmneuInBlank0";
            this._CmneuInBlank0.Size = new System.Drawing.Size(116, 6);
            // 
            // _CmenuInExecute
            // 
            this._CmenuInExecute.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuInExecute.Image")));
            this._CmenuInExecute.Name = "_CmenuInExecute";
            this._CmenuInExecute.Size = new System.Drawing.Size(119, 22);
            this._CmenuInExecute.Text = "Execute";
            this._CmenuInExecute.Click += new System.EventHandler(this._MenuExecute_Click);
            // 
            // _CmenuInBlank1
            // 
            this._CmenuInBlank1.Name = "_CmenuInBlank1";
            this._CmenuInBlank1.Size = new System.Drawing.Size(116, 6);
            // 
            // _CmenuInCommit
            // 
            this._CmenuInCommit.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuInCommit.Image")));
            this._CmenuInCommit.Name = "_CmenuInCommit";
            this._CmenuInCommit.Size = new System.Drawing.Size(119, 22);
            this._CmenuInCommit.Text = "Commit";
            this._CmenuInCommit.Click += new System.EventHandler(this._MenuCommit_Click);
            // 
            // _CmenuInRollback
            // 
            this._CmenuInRollback.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuInRollback.Image")));
            this._CmenuInRollback.Name = "_CmenuInRollback";
            this._CmenuInRollback.Size = new System.Drawing.Size(119, 22);
            this._CmenuInRollback.Text = "Rollback";
            this._CmenuInRollback.Click += new System.EventHandler(this._MenuRollback_Click);
            // 
            // __GridOut
            // 
            this.@__GridOut.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.@__GridOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.@__GridOut.ContextMenuStrip = this._CmenuOut;
            this.@__GridOut.EnableHeadersVisualStyles = false;
            this.@__GridOut.Location = new System.Drawing.Point(218, 21);
            this.@__GridOut.MultiSelect = false;
            this.@__GridOut.Name = "__GridOut";
            this.@__GridOut.Size = new System.Drawing.Size(153, 162);
            this.@__GridOut.TabIndex = 1;
            // 
            // _CmenuOut
            // 
            this._CmenuOut.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuOutCopy});
            this._CmenuOut.Name = "_CmenuOut";
            this._CmenuOut.Size = new System.Drawing.Size(103, 26);
            // 
            // _CmenuOutCopy
            // 
            this._CmenuOutCopy.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuOutCopy.Image")));
            this._CmenuOutCopy.Name = "_CmenuOutCopy";
            this._CmenuOutCopy.Size = new System.Drawing.Size(102, 22);
            this._CmenuOutCopy.Text = "Copy";
            this._CmenuOutCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // __RtfOut
            // 
            this.@__RtfOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.@__RtfOut.ContextMenuStrip = this._CmenuOut;
            this.@__RtfOut.Location = new System.Drawing.Point(29, 21);
            this.@__RtfOut.Name = "__RtfOut";
            this.@__RtfOut.ReadOnly = true;
            this.@__RtfOut.ShortcutsEnabled = false;
            this.@__RtfOut.Size = new System.Drawing.Size(155, 162);
            this.@__RtfOut.TabIndex = 1;
            this.@__RtfOut.Text = "";
            this.@__RtfOut.WordWrap = false;
            // 
            // _WaitMain
            // 
            this._WaitMain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._WaitMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._WaitMain.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._WaitMain.Location = new System.Drawing.Point(566, 46);
            this._WaitMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._WaitMain.Name = "_WaitMain";
            this._WaitMain.Size = new System.Drawing.Size(450, 63);
            this._WaitMain.TabIndex = 4;
            this._WaitMain.Visible = false;
            // 
            // _GotoSearch
            // 
            this._GotoSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._GotoSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._GotoSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GotoSearch.Line = 0;
            this._GotoSearch.Location = new System.Drawing.Point(566, 46);
            this._GotoSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._GotoSearch.Name = "_GotoSearch";
            this._GotoSearch.Pinned = false;
            this._GotoSearch.Size = new System.Drawing.Size(452, 125);
            this._GotoSearch.TabIndex = 2;
            this._GotoSearch.Visible = false;
            this._GotoSearch.GoTo += new System.EventHandler(this._GotoSearch_GoTo);
            this._GotoSearch.LineChanged += new Robbiblubber.Util.Controls.GoToFeedbackEventHandler(this._GotoSearch_LineChanged);
            // 
            // _ReplaceSearch
            // 
            this._ReplaceSearch.AllowReplace = true;
            this._ReplaceSearch.AllowReplaceAll = true;
            this._ReplaceSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ReplaceSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ReplaceSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ReplaceSearch.Location = new System.Drawing.Point(566, 46);
            this._ReplaceSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._ReplaceSearch.Name = "_ReplaceSearch";
            this._ReplaceSearch.Pinned = false;
            this._ReplaceSearch.ReplaceText = "";
            this._ReplaceSearch.SearchText = "";
            this._ReplaceSearch.Size = new System.Drawing.Size(452, 181);
            this._ReplaceSearch.TabIndex = 3;
            this._ReplaceSearch.Visible = false;
            this._ReplaceSearch.SearchTextChanged += new System.EventHandler(this._ReplaceSearch_SearchTextChanged);
            this._ReplaceSearch.Search += new System.EventHandler(this._ReplaceSearch_Search);
            this._ReplaceSearch.Replace += new System.EventHandler(this._ReplaceSearch_Replace);
            this._ReplaceSearch.ReplaceAll += new System.EventHandler(this._ReplaceSearch_ReplaceAll);
            // 
            // _FindSearch
            // 
            this._FindSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._FindSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FindSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FindSearch.Location = new System.Drawing.Point(566, 46);
            this._FindSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._FindSearch.Name = "_FindSearch";
            this._FindSearch.Pinned = false;
            this._FindSearch.SearchText = "";
            this._FindSearch.Size = new System.Drawing.Size(452, 125);
            this._FindSearch.TabIndex = 1;
            this._FindSearch.Visible = false;
            this._FindSearch.Search += new System.EventHandler(this._FindSearch_Search);
            // 
            // _CmenuDisconnect
            // 
            this._CmenuDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDisconnect.Image")));
            this._CmenuDisconnect.Name = "_CmenuDisconnect";
            this._CmenuDisconnect.Size = new System.Drawing.Size(180, 22);
            this._CmenuDisconnect.Tag = "sqlfwx::menu.disconnect";
            this._CmenuDisconnect.Text = "Disconnect";
            this._CmenuDisconnect.Click += new System.EventHandler(this._MenuDisconnect_Click);
            // 
            // _CmenuTreeBlank0
            // 
            this._CmenuTreeBlank0.Name = "_CmenuTreeBlank0";
            this._CmenuTreeBlank0.Size = new System.Drawing.Size(130, 6);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 563);
            this.Controls.Add(this._WaitMain);
            this.Controls.Add(this._GotoSearch);
            this.Controls.Add(this._ReplaceSearch);
            this.Controls.Add(this._FindSearch);
            this.Controls.Add(this._SplitMain);
            this.Controls.Add(this._MenuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMain";
            this.Tag = "";
            this.Text = "rqoon";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this._MenuMain.ResumeLayout(false);
            this._MenuMain.PerformLayout();
            this._SplitMain.Panel1.ResumeLayout(false);
            this._SplitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._SplitMain)).EndInit();
            this._SplitMain.ResumeLayout(false);
            this._CmenuTree.ResumeLayout(false);
            this._SplitSQL.Panel1.ResumeLayout(false);
            this._SplitSQL.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._SplitSQL)).EndInit();
            this._SplitSQL.ResumeLayout(false);
            this._CmenuIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.@__GridOut)).EndInit();
            this._CmenuOut.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _MenuMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuDatabases;
        private System.Windows.Forms.ToolStripMenuItem _MenuConnect;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuDisconnect;
        private System.Windows.Forms.SplitContainer _SplitMain;
        private System.Windows.Forms.TreeView _TreeStructure;
        private System.Windows.Forms.SplitContainer _SplitSQL;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuUndo;
        private System.Windows.Forms.ToolStripMenuItem _MenuRedo;
        private System.Windows.Forms.ToolStripSeparator _MenuEditBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCut;
        private System.Windows.Forms.ToolStripMenuItem _MenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _MenuPaste;
        private System.Windows.Forms.ToolStripSeparator _MenuEditBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuSelectAll;
        private System.Windows.Forms.ToolStripMenuItem _MenuDeselectAll;
        private System.Windows.Forms.ToolStripSeparator _MenuEditBlank2;
        private System.Windows.Forms.ToolStripMenuItem _MenuClear;
        private System.Windows.Forms.ToolStripMenuItem _MenuView;
        private System.Windows.Forms.ToolStripMenuItem _MenuText;
        private System.Windows.Forms.ToolStripMenuItem _MenuTable;
        private System.Windows.Forms.ToolStripMenuItem _MenuConsole;
        private System.Windows.Forms.ToolStripMenuItem _MenuExecute;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpen;
        private System.Windows.Forms.ToolStripMenuItem _MenuSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowExplorer;
        private System.Windows.Forms.ToolStripSeparator _MenuViewBlank0;
        private System.Windows.Forms.ImageList _IlistTree;
        private System.Windows.Forms.ContextMenuStrip _CmenuTree;
        private System.Windows.Forms.ToolStripMenuItem _CmenuRefresh;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem _MenuHistory;
        private System.Windows.Forms.ToolStripMenuItem _MenuBrowseHistory;
        private System.Windows.Forms.ToolStripMenuItem _MenuDumpHistory;
        private System.Windows.Forms.ToolStripSeparator _MenuHistoryBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCleanUpHistory;
        private System.Windows.Forms.ToolStripMenuItem _MenuExport;
        private System.Windows.Forms.ToolStripMenuItem _MenuFavoriteSQL;
        private System.Windows.Forms.ToolStripSeparator _MenuToolsBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ToolStripSeparator _MenuConsoleBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCommit;
        private System.Windows.Forms.ToolStripMenuItem _MenuRollback;
        internal System.Windows.Forms.RichTextBox __RtfIn;
        internal System.Windows.Forms.RichTextBox __RtfOut;
        internal System.Windows.Forms.DataGridView __GridOut;
        private System.Windows.Forms.ToolStripMenuItem _MenuFind;
        private System.Windows.Forms.ToolStripMenuItem _MenuFindNext;
        private System.Windows.Forms.ToolStripMenuItem _MenuReplace;
        private System.Windows.Forms.ToolStripMenuItem _MenuGoTo;
        private System.Windows.Forms.ToolStripSeparator _MenuEditBlank3;
        private System.Windows.Forms.ToolStripMenuItem _MenuExplorer;
        private System.Windows.Forms.ToolStripMenuItem _MenuExplorerFind;
        private System.Windows.Forms.ToolStripMenuItem _MenuExplorerFindNext;
        private Robbiblubber.Util.Controls.FindControl _FindSearch;
        private Robbiblubber.Util.Controls.GoToControl _GotoSearch;
        private Robbiblubber.Util.Controls.ReplaceControl _ReplaceSearch;
        private System.Windows.Forms.ContextMenuStrip _CmenuOut;
        private System.Windows.Forms.ToolStripMenuItem _CmenuOutCopy;
        private System.Windows.Forms.ContextMenuStrip _CmenuIn;
        private System.Windows.Forms.ToolStripMenuItem _CmenuInCut;
        private System.Windows.Forms.ToolStripMenuItem _CmenuInCopy;
        private System.Windows.Forms.ToolStripMenuItem _CmenuInPaste;
        private System.Windows.Forms.ToolStripSeparator _CmneuInBlank0;
        private System.Windows.Forms.ToolStripMenuItem _CmenuInExecute;
        private System.Windows.Forms.ToolStripSeparator _CmenuInBlank1;
        private System.Windows.Forms.ToolStripMenuItem _CmenuInCommit;
        private System.Windows.Forms.ToolStripMenuItem _CmenuInRollback;
        private Robbiblubber.Util.Controls.WaitControl _WaitMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbort;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
        private System.Windows.Forms.ToolStripMenuItem _MenuRecent;
        private System.Windows.Forms.ToolStripMenuItem _MenuExecuteCurrent;
        private System.Windows.Forms.ToolStripMenuItem _MenuExecuteToCurrent;
        private System.Windows.Forms.ToolStripMenuItem _MenuEexecuteFromCurrent;
        private System.Windows.Forms.ToolStripSeparator _MenuHistoryBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuDatabase;
        private System.Windows.Forms.ToolStripMenuItem _MenuRefersh;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDisconnect;
        private System.Windows.Forms.ToolStripSeparator _CmenuTreeBlank0;
    }
}

