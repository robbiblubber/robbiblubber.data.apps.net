﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    /// <summary>This interface defines basic program behavior.</summary>
    public interface IProgram
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the program supports an explorer.</summary>
        bool SupportsExplorer { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Initializes the program.</summary>
        void Init();


        /// <summary>Shows a splash window.</summary>
        /// <returns>Splash window.</returns>
        ISplashWindow ShowSplash();


        /// <summary>Shows an about window.</summary>
        /// <returns>Result.</returns>
        DialogResult ShowAbout();
    }
}
