﻿namespace Robbiblubber.Data.Apps.SQLWorkspace
{
    partial class TextEditorSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextEditorSettingsControl));
            this._LabelInput = new System.Windows.Forms.Label();
            this._LabelInputIcon = new System.Windows.Forms.Label();
            this._PanelInput = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this._CheckExecuteCtrlEnter = new System.Windows.Forms.CheckBox();
            this._CheckExecuteAltEnter = new System.Windows.Forms.CheckBox();
            this._LinkInputTextColor = new Robbiblubber.Util.Controls.LinkControl();
            this._LinkInputBgColor = new Robbiblubber.Util.Controls.LinkControl();
            this._LinkInputFont = new Robbiblubber.Util.Controls.LinkControl();
            this._PanelInputText = new System.Windows.Forms.Panel();
            this._LabelInputText = new System.Windows.Forms.Label();
            this._PanelOutputText = new System.Windows.Forms.Panel();
            this._LabelOutputText = new System.Windows.Forms.Label();
            this._LabelOutput = new System.Windows.Forms.Label();
            this._LabelOutputIcon = new System.Windows.Forms.Label();
            this._PanelOutput = new System.Windows.Forms.Panel();
            this._LinkOutputTextColor = new Robbiblubber.Util.Controls.LinkControl();
            this._LinkOutputBgColor = new Robbiblubber.Util.Controls.LinkControl();
            this._LinkOutputFont = new Robbiblubber.Util.Controls.LinkControl();
            this._PanelInput.SuspendLayout();
            this._PanelInputText.SuspendLayout();
            this._PanelOutputText.SuspendLayout();
            this._PanelOutput.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelInput
            // 
            this._LabelInput.AutoSize = true;
            this._LabelInput.Location = new System.Drawing.Point(44, 27);
            this._LabelInput.Name = "_LabelInput";
            this._LabelInput.Size = new System.Drawing.Size(88, 17);
            this._LabelInput.TabIndex = 0;
            this._LabelInput.Tag = "sqlfwx::udiag.txtset.input";
            this._LabelInput.Text = "&Input Window";
            // 
            // _LabelInputIcon
            // 
            this._LabelInputIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelInputIcon.Image")));
            this._LabelInputIcon.Location = new System.Drawing.Point(21, 26);
            this._LabelInputIcon.Name = "_LabelInputIcon";
            this._LabelInputIcon.Size = new System.Drawing.Size(24, 21);
            this._LabelInputIcon.TabIndex = 0;
            // 
            // _PanelInput
            // 
            this._PanelInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelInput.Controls.Add(this.label2);
            this._PanelInput.Controls.Add(this._CheckExecuteCtrlEnter);
            this._PanelInput.Controls.Add(this._CheckExecuteAltEnter);
            this._PanelInput.Controls.Add(this._LinkInputTextColor);
            this._PanelInput.Controls.Add(this._LinkInputBgColor);
            this._PanelInput.Controls.Add(this._LinkInputFont);
            this._PanelInput.Location = new System.Drawing.Point(142, 50);
            this._PanelInput.Name = "_PanelInput";
            this._PanelInput.Size = new System.Drawing.Size(533, 108);
            this._PanelInput.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(216, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 3;
            this.label2.Tag = "sqlfwx::udiag.txtset.exec";
            this.label2.Text = "&Execute Shortcuts:";
            // 
            // _CheckExecuteCtrlEnter
            // 
            this._CheckExecuteCtrlEnter.AutoSize = true;
            this._CheckExecuteCtrlEnter.Location = new System.Drawing.Point(336, 38);
            this._CheckExecuteCtrlEnter.Name = "_CheckExecuteCtrlEnter";
            this._CheckExecuteCtrlEnter.Size = new System.Drawing.Size(102, 21);
            this._CheckExecuteCtrlEnter.TabIndex = 3;
            this._CheckExecuteCtrlEnter.Tag = "sqlfwx::udiag.txtset.ctrlenter";
            this._CheckExecuteCtrlEnter.Text = "[Ctrl]+[&Enter]";
            this._CheckExecuteCtrlEnter.UseVisualStyleBackColor = true;
            // 
            // _CheckExecuteAltEnter
            // 
            this._CheckExecuteAltEnter.AutoSize = true;
            this._CheckExecuteAltEnter.Location = new System.Drawing.Point(336, 62);
            this._CheckExecuteAltEnter.Name = "_CheckExecuteAltEnter";
            this._CheckExecuteAltEnter.Size = new System.Drawing.Size(97, 21);
            this._CheckExecuteAltEnter.TabIndex = 4;
            this._CheckExecuteAltEnter.Tag = "sqlfwx::udiag.txtset.altenter";
            this._CheckExecuteAltEnter.Text = "[&Alt]+[Enter]";
            this._CheckExecuteAltEnter.UseVisualStyleBackColor = true;
            // 
            // _LinkInputTextColor
            // 
            this._LinkInputTextColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkInputTextColor.DisabledColor = System.Drawing.Color.Gray;
            this._LinkInputTextColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkInputTextColor.LinkText = "Text Color...";
            this._LinkInputTextColor.Location = new System.Drawing.Point(29, 75);
            this._LinkInputTextColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkInputTextColor.Name = "_LinkInputTextColor";
            this._LinkInputTextColor.Size = new System.Drawing.Size(75, 21);
            this._LinkInputTextColor.TabIndex = 2;
            this._LinkInputTextColor.Tag = "sqlfwx::udiag.txtset.setfg";
            this._LinkInputTextColor.Underline = false;
            this._LinkInputTextColor.Click += new System.EventHandler(this._LinkInputTextColor_Click);
            // 
            // _LinkInputBgColor
            // 
            this._LinkInputBgColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkInputBgColor.DisabledColor = System.Drawing.Color.Gray;
            this._LinkInputBgColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkInputBgColor.LinkText = "Background Color...";
            this._LinkInputBgColor.Location = new System.Drawing.Point(29, 56);
            this._LinkInputBgColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkInputBgColor.Name = "_LinkInputBgColor";
            this._LinkInputBgColor.Size = new System.Drawing.Size(118, 21);
            this._LinkInputBgColor.TabIndex = 1;
            this._LinkInputBgColor.Tag = "sqlfwx::udiag.txtset.setbg";
            this._LinkInputBgColor.Underline = false;
            this._LinkInputBgColor.Click += new System.EventHandler(this._LinkInputBgColor_Click);
            // 
            // _LinkInputFont
            // 
            this._LinkInputFont.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkInputFont.DisabledColor = System.Drawing.Color.Gray;
            this._LinkInputFont.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkInputFont.LinkText = "Font...";
            this._LinkInputFont.Location = new System.Drawing.Point(29, 38);
            this._LinkInputFont.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkInputFont.Name = "_LinkInputFont";
            this._LinkInputFont.Size = new System.Drawing.Size(57, 21);
            this._LinkInputFont.TabIndex = 0;
            this._LinkInputFont.Tag = "sqlfwx::udiag.txtset.setfont";
            this._LinkInputFont.Underline = false;
            this._LinkInputFont.Click += new System.EventHandler(this._LinkInputFont_Click);
            // 
            // _PanelInputText
            // 
            this._PanelInputText.BackColor = System.Drawing.Color.White;
            this._PanelInputText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelInputText.Controls.Add(this._LabelInputText);
            this._PanelInputText.Location = new System.Drawing.Point(24, 50);
            this._PanelInputText.Name = "_PanelInputText";
            this._PanelInputText.Size = new System.Drawing.Size(119, 108);
            this._PanelInputText.TabIndex = 0;
            // 
            // _LabelInputText
            // 
            this._LabelInputText.AutoSize = true;
            this._LabelInputText.Location = new System.Drawing.Point(10, 9);
            this._LabelInputText.Name = "_LabelInputText";
            this._LabelInputText.Size = new System.Drawing.Size(31, 17);
            this._LabelInputText.TabIndex = 0;
            this._LabelInputText.Tag = "sqlfwx::udiag.txtset.text";
            this._LabelInputText.Text = "Text";
            // 
            // _PanelOutputText
            // 
            this._PanelOutputText.BackColor = System.Drawing.Color.White;
            this._PanelOutputText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelOutputText.Controls.Add(this._LabelOutputText);
            this._PanelOutputText.Location = new System.Drawing.Point(24, 200);
            this._PanelOutputText.Name = "_PanelOutputText";
            this._PanelOutputText.Size = new System.Drawing.Size(119, 108);
            this._PanelOutputText.TabIndex = 5;
            // 
            // _LabelOutputText
            // 
            this._LabelOutputText.AutoSize = true;
            this._LabelOutputText.Location = new System.Drawing.Point(10, 9);
            this._LabelOutputText.Name = "_LabelOutputText";
            this._LabelOutputText.Size = new System.Drawing.Size(31, 17);
            this._LabelOutputText.TabIndex = 5;
            this._LabelOutputText.Tag = "sqlfwx::udiag.txtset.text";
            this._LabelOutputText.Text = "Text";
            // 
            // _LabelOutput
            // 
            this._LabelOutput.AutoSize = true;
            this._LabelOutput.Location = new System.Drawing.Point(44, 177);
            this._LabelOutput.Name = "_LabelOutput";
            this._LabelOutput.Size = new System.Drawing.Size(99, 17);
            this._LabelOutput.TabIndex = 5;
            this._LabelOutput.Tag = "sqlfwx::udiag.txtset.output";
            this._LabelOutput.Text = "&Output Window";
            // 
            // _LabelOutputIcon
            // 
            this._LabelOutputIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelOutputIcon.Image")));
            this._LabelOutputIcon.Location = new System.Drawing.Point(21, 176);
            this._LabelOutputIcon.Name = "_LabelOutputIcon";
            this._LabelOutputIcon.Size = new System.Drawing.Size(24, 21);
            this._LabelOutputIcon.TabIndex = 5;
            // 
            // _PanelOutput
            // 
            this._PanelOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelOutput.Controls.Add(this._LinkOutputTextColor);
            this._PanelOutput.Controls.Add(this._LinkOutputBgColor);
            this._PanelOutput.Controls.Add(this._LinkOutputFont);
            this._PanelOutput.Location = new System.Drawing.Point(142, 200);
            this._PanelOutput.Name = "_PanelOutput";
            this._PanelOutput.Size = new System.Drawing.Size(533, 108);
            this._PanelOutput.TabIndex = 5;
            // 
            // _LinkOutputTextColor
            // 
            this._LinkOutputTextColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkOutputTextColor.DisabledColor = System.Drawing.Color.Gray;
            this._LinkOutputTextColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkOutputTextColor.LinkText = "Text Color...";
            this._LinkOutputTextColor.Location = new System.Drawing.Point(29, 74);
            this._LinkOutputTextColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkOutputTextColor.Name = "_LinkOutputTextColor";
            this._LinkOutputTextColor.Size = new System.Drawing.Size(75, 21);
            this._LinkOutputTextColor.TabIndex = 6;
            this._LinkOutputTextColor.Tag = "sqlfwx::udiag.txtset.setfg";
            this._LinkOutputTextColor.Underline = false;
            this._LinkOutputTextColor.Click += new System.EventHandler(this._LinkOutputTextColor_Click);
            // 
            // _LinkOutputBgColor
            // 
            this._LinkOutputBgColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkOutputBgColor.DisabledColor = System.Drawing.Color.Gray;
            this._LinkOutputBgColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkOutputBgColor.LinkText = "Background Color...";
            this._LinkOutputBgColor.Location = new System.Drawing.Point(29, 55);
            this._LinkOutputBgColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkOutputBgColor.Name = "_LinkOutputBgColor";
            this._LinkOutputBgColor.Size = new System.Drawing.Size(118, 21);
            this._LinkOutputBgColor.TabIndex = 6;
            this._LinkOutputBgColor.Tag = "sqlfwx::udiag.txtset.setbg";
            this._LinkOutputBgColor.Underline = false;
            this._LinkOutputBgColor.Click += new System.EventHandler(this._LinkOutputBgColor_Click);
            // 
            // _LinkOutputFont
            // 
            this._LinkOutputFont.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkOutputFont.DisabledColor = System.Drawing.Color.Gray;
            this._LinkOutputFont.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkOutputFont.LinkText = "Font...";
            this._LinkOutputFont.Location = new System.Drawing.Point(29, 37);
            this._LinkOutputFont.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkOutputFont.Name = "_LinkOutputFont";
            this._LinkOutputFont.Size = new System.Drawing.Size(57, 21);
            this._LinkOutputFont.TabIndex = 6;
            this._LinkOutputFont.Tag = "sqlfwx::udiag.txtset.setfont";
            this._LinkOutputFont.Underline = false;
            this._LinkOutputFont.Click += new System.EventHandler(this._LinkOutputFont_Click);
            // 
            // TextEditorSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._PanelOutputText);
            this.Controls.Add(this._LabelOutput);
            this.Controls.Add(this._LabelOutputIcon);
            this.Controls.Add(this._PanelOutput);
            this.Controls.Add(this._PanelInputText);
            this.Controls.Add(this._LabelInput);
            this.Controls.Add(this._LabelInputIcon);
            this.Controls.Add(this._PanelInput);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "TextEditorSettingsControl";
            this.Size = new System.Drawing.Size(697, 349);
            this._PanelInput.ResumeLayout(false);
            this._PanelInput.PerformLayout();
            this._PanelInputText.ResumeLayout(false);
            this._PanelInputText.PerformLayout();
            this._PanelOutputText.ResumeLayout(false);
            this._PanelOutputText.PerformLayout();
            this._PanelOutput.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelInput;
        private System.Windows.Forms.Label _LabelInputIcon;
        private System.Windows.Forms.Panel _PanelInput;
        private System.Windows.Forms.Panel _PanelInputText;
        private System.Windows.Forms.Label _LabelInputText;
        private System.Windows.Forms.CheckBox _CheckExecuteAltEnter;
        private Robbiblubber.Util.Controls.LinkControl _LinkInputTextColor;
        private Robbiblubber.Util.Controls.LinkControl _LinkInputBgColor;
        private Robbiblubber.Util.Controls.LinkControl _LinkInputFont;
        private System.Windows.Forms.Panel _PanelOutputText;
        private System.Windows.Forms.Label _LabelOutputText;
        private System.Windows.Forms.Label _LabelOutput;
        private System.Windows.Forms.Label _LabelOutputIcon;
        private System.Windows.Forms.Panel _PanelOutput;
        private Robbiblubber.Util.Controls.LinkControl _LinkOutputTextColor;
        private Robbiblubber.Util.Controls.LinkControl _LinkOutputBgColor;
        private Robbiblubber.Util.Controls.LinkControl _LinkOutputFont;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox _CheckExecuteCtrlEnter;
    }
}
