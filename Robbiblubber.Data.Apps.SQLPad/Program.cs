﻿using System;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Data.Apps.SQLWorkspace;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Apps.SQLPad
{
    /// <summary>Program class.</summary>
    internal class Program: IProgram
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="args">Command line arguments.</param>
        [STAThread]
        static void Main(string[] args)
        {
            foreach(string i in args)
            {
                if(File.Exists(i)) { WorkspaceApplication.__FileName = i; break; }
            }

            WorkspaceApplication.Run(new Program());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProgram                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the program supports an explorer.</summary>
        public bool SupportsExplorer
        {
            get { return false; }
        }


        /// <summary>Initializes the program.</summary>
        public void Init()
        {
            PathOp.ApplicationPart = "robbiblubber.org/SQLPad";
            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");
        }


        /// <summary>Shows a splash window.</summary>
        /// <returns>Splash window.</returns>
        public ISplashWindow ShowSplash()
        {
            FormSplash f = new FormSplash();
            f.Show();

            return f;
        }


        /// <summary>Shows an about window.</summary>
        /// <returns>About window</returns>
        public DialogResult ShowAbout()
        {
            FormAbout f = new FormAbout();
            return f.ShowDialog();
        }
    }
}
