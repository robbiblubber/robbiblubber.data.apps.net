﻿namespace Robbiblubber.Data.Apps.SQLConsole
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this._ChkDebug = new System.Windows.Forms.CheckBox();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._LcboLanguage = new Robbiblubber.Util.Localization.Controls.LocaleComboBox();
            this._LabelLanguage = new System.Windows.Forms.Label();
            this._LabelMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _ChkDebug
            // 
            this._ChkDebug.AutoSize = true;
            this._ChkDebug.Location = new System.Drawing.Point(43, 110);
            this._ChkDebug.Name = "_ChkDebug";
            this._ChkDebug.Size = new System.Drawing.Size(372, 21);
            this._ChkDebug.TabIndex = 14;
            this._ChkDebug.Tag = "debugx::settings.showdebug";
            this._ChkDebug.Text = "   Enable &debug features (show Debug menu in menu strip)";
            this._ChkDebug.UseVisualStyleBackColor = true;
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(115, 153);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(147, 29);
            this._ButtonOK.TabIndex = 11;
            this._ButtonOK.Tag = "ledx::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(268, 153);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 12;
            this._ButtonCancel.Tag = "ledx::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _LcboLanguage
            // 
            this._LcboLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LcboLanguage.Location = new System.Drawing.Point(43, 37);
            this._LcboLanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LcboLanguage.Name = "_LcboLanguage";
            this._LcboLanguage.Size = new System.Drawing.Size(372, 31);
            this._LcboLanguage.TabIndex = 10;
            this._LcboLanguage.Value = null;
            // 
            // _LabelLanguage
            // 
            this._LabelLanguage.AutoSize = true;
            this._LabelLanguage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLanguage.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLanguage.Location = new System.Drawing.Point(40, 18);
            this._LabelLanguage.Name = "_LabelLanguage";
            this._LabelLanguage.Size = new System.Drawing.Size(135, 13);
            this._LabelLanguage.TabIndex = 9;
            this._LabelLanguage.Tag = "ledx::settings.language.text";
            this._LabelLanguage.Text = "User Interface &Language:";
            // 
            // _LabelMessage
            // 
            this._LabelMessage.AutoSize = true;
            this._LabelMessage.Location = new System.Drawing.Point(40, 51);
            this._LabelMessage.Name = "_LabelMessage";
            this._LabelMessage.Size = new System.Drawing.Size(262, 17);
            this._LabelMessage.TabIndex = 13;
            this._LabelMessage.Text = "Language options are currently unavailable.\r\n";
            this._LabelMessage.Visible = false;
            // 
            // FormSettings
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(459, 208);
            this.Controls.Add(this._ChkDebug);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._LcboLanguage);
            this.Controls.Add(this._LabelLanguage);
            this.Controls.Add(this._LabelMessage);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _ChkDebug;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonCancel;
        private Robbiblubber.Util.Localization.Controls.LocaleComboBox _LcboLanguage;
        private System.Windows.Forms.Label _LabelLanguage;
        private System.Windows.Forms.Label _LabelMessage;
    }
}