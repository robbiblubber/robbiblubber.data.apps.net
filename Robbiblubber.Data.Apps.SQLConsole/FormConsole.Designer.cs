﻿namespace Robbiblubber.Data.Apps.SQLConsole
{
    partial class FormConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsole));
            this._MenuMain = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDatabases = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuConnect = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRun = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBrowseHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDumpHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFavoriteSQL = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHistoryBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCleanup = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuToolsBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._TimerRun = new System.Windows.Forms.Timer(this.components);
            this._Console = new Robbiblubber.Util.Controls.ConsoleControl();
            this._MenuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuMain
            // 
            this._MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuTools,
            this._MenuHelp});
            this._MenuMain.Location = new System.Drawing.Point(0, 0);
            this._MenuMain.Name = "_MenuMain";
            this._MenuMain.Size = new System.Drawing.Size(992, 24);
            this._MenuMain.TabIndex = 1;
            this._MenuMain.Text = "menuStrip1";
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDatabases,
            this._MenuConnect,
            this._MenuDisconnect,
            this._MenuFileBlank0,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 20);
            this._MenuFile.Tag = "sqlfwx::menu.file";
            this._MenuFile.Text = "&File";
            // 
            // _MenuDatabases
            // 
            this._MenuDatabases.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDatabases.Image")));
            this._MenuDatabases.Name = "_MenuDatabases";
            this._MenuDatabases.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this._MenuDatabases.Size = new System.Drawing.Size(180, 22);
            this._MenuDatabases.Tag = "sqlfwx::menu.databases";
            this._MenuDatabases.Text = "&Databases...";
            this._MenuDatabases.Click += new System.EventHandler(this._MenuDatabases_Click);
            // 
            // _MenuConnect
            // 
            this._MenuConnect.Image = ((System.Drawing.Image)(resources.GetObject("_MenuConnect.Image")));
            this._MenuConnect.Name = "_MenuConnect";
            this._MenuConnect.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F12)));
            this._MenuConnect.Size = new System.Drawing.Size(180, 22);
            this._MenuConnect.Tag = "sqlfwx::menu.connect";
            this._MenuConnect.Text = "&Connect...";
            this._MenuConnect.Click += new System.EventHandler(this._MenuConnect_Click);
            // 
            // _MenuDisconnect
            // 
            this._MenuDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDisconnect.Image")));
            this._MenuDisconnect.Name = "_MenuDisconnect";
            this._MenuDisconnect.Size = new System.Drawing.Size(180, 22);
            this._MenuDisconnect.Tag = "sqlfwx::menu.disconnect";
            this._MenuDisconnect.Text = "Di&sconnect";
            this._MenuDisconnect.Click += new System.EventHandler(this._MenuDisconnect_Click);
            // 
            // _MenuFileBlank0
            // 
            this._MenuFileBlank0.Name = "_MenuFileBlank0";
            this._MenuFileBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.Size = new System.Drawing.Size(180, 22);
            this._MenuExit.Tag = "sqlfwx::menu.exit";
            this._MenuExit.Text = "E&xit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuCut,
            this._MenuCopy,
            this._MenuPaste});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 20);
            this._MenuEdit.Tag = "sqlfwx::menu.edit";
            this._MenuEdit.Text = "&Edit";
            this._MenuEdit.DropDownOpening += new System.EventHandler(this._MenuEdit_DropDownOpening);
            // 
            // _MenuCut
            // 
            this._MenuCut.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCut.Image")));
            this._MenuCut.Name = "_MenuCut";
            this._MenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._MenuCut.Size = new System.Drawing.Size(144, 22);
            this._MenuCut.Tag = "sqlfwx::menu.cut";
            this._MenuCut.Text = "C&ut";
            this._MenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _MenuCopy
            // 
            this._MenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCopy.Image")));
            this._MenuCopy.Name = "_MenuCopy";
            this._MenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuCopy.Size = new System.Drawing.Size(144, 22);
            this._MenuCopy.Tag = "sqlfwx::menu.copy";
            this._MenuCopy.Text = "&Copy";
            this._MenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _MenuPaste
            // 
            this._MenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPaste.Image")));
            this._MenuPaste.Name = "_MenuPaste";
            this._MenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._MenuPaste.Size = new System.Drawing.Size(144, 22);
            this._MenuPaste.Tag = "sqlfwx::menu.paste";
            this._MenuPaste.Text = "&Paste";
            this._MenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuRun,
            this._MenuHistory,
            this._MenuToolsBlank0,
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(46, 20);
            this._MenuTools.Tag = "sqlfwx::menu.tools";
            this._MenuTools.Text = "&Tools";
            // 
            // _MenuRun
            // 
            this._MenuRun.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRun.Image")));
            this._MenuRun.Name = "_MenuRun";
            this._MenuRun.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this._MenuRun.Size = new System.Drawing.Size(145, 22);
            this._MenuRun.Tag = "sqlfwx::menu.run";
            this._MenuRun.Text = "&Run...";
            this._MenuRun.Click += new System.EventHandler(this._MenuRun_Click);
            // 
            // _MenuHistory
            // 
            this._MenuHistory.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuBrowseHistory,
            this._MenuDumpHistory,
            this._MenuFavoriteSQL,
            this._MenuHistoryBlank0,
            this._MenuCleanup});
            this._MenuHistory.Name = "_MenuHistory";
            this._MenuHistory.Size = new System.Drawing.Size(145, 22);
            this._MenuHistory.Tag = "sqlfwx::menu.history";
            this._MenuHistory.Text = "History";
            // 
            // _MenuBrowseHistory
            // 
            this._MenuBrowseHistory.Image = ((System.Drawing.Image)(resources.GetObject("_MenuBrowseHistory.Image")));
            this._MenuBrowseHistory.Name = "_MenuBrowseHistory";
            this._MenuBrowseHistory.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this._MenuBrowseHistory.Size = new System.Drawing.Size(192, 22);
            this._MenuBrowseHistory.Tag = "sqlfwx::menu.browsehistory";
            this._MenuBrowseHistory.Text = "&Browse,,,";
            this._MenuBrowseHistory.Click += new System.EventHandler(this._MenuBrowseHistory_Click);
            // 
            // _MenuDumpHistory
            // 
            this._MenuDumpHistory.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDumpHistory.Image")));
            this._MenuDumpHistory.Name = "_MenuDumpHistory";
            this._MenuDumpHistory.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this._MenuDumpHistory.Size = new System.Drawing.Size(192, 22);
            this._MenuDumpHistory.Tag = "sqlfwx::menu.dumphistory";
            this._MenuDumpHistory.Text = "&Dump...";
            this._MenuDumpHistory.Click += new System.EventHandler(this._MenuDumpHistory_Click);
            // 
            // _MenuFavoriteSQL
            // 
            this._MenuFavoriteSQL.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFavoriteSQL.Image")));
            this._MenuFavoriteSQL.Name = "_MenuFavoriteSQL";
            this._MenuFavoriteSQL.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this._MenuFavoriteSQL.Size = new System.Drawing.Size(192, 22);
            this._MenuFavoriteSQL.Tag = "sqlfwx::menu.favoritesql";
            this._MenuFavoriteSQL.Text = "&Favorite SQL...";
            this._MenuFavoriteSQL.Click += new System.EventHandler(this._MenuFavoriteSQL_Click);
            // 
            // _MenuHistoryBlank0
            // 
            this._MenuHistoryBlank0.Name = "_MenuHistoryBlank0";
            this._MenuHistoryBlank0.Size = new System.Drawing.Size(189, 6);
            // 
            // _MenuCleanup
            // 
            this._MenuCleanup.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCleanup.Image")));
            this._MenuCleanup.Name = "_MenuCleanup";
            this._MenuCleanup.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this._MenuCleanup.Size = new System.Drawing.Size(192, 22);
            this._MenuCleanup.Tag = "sqlfwx::menu.cleanup";
            this._MenuCleanup.Text = "&Clean Up...";
            this._MenuCleanup.Click += new System.EventHandler(this._MenuCleanup_Click);
            // 
            // _MenuToolsBlank0
            // 
            this._MenuToolsBlank0.Name = "_MenuToolsBlank0";
            this._MenuToolsBlank0.Size = new System.Drawing.Size(142, 6);
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(145, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "&Debug";
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.Size = new System.Drawing.Size(157, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "Start &Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.Size = new System.Drawing.Size(157, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "Stop &Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(154, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.Size = new System.Drawing.Size(157, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "Save &Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.Size = new System.Drawing.Size(157, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.Size = new System.Drawing.Size(157, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(145, 22);
            this._MenuSettings.Tag = "sqlfwx::menu.settings";
            this._MenuSettings.Text = "&Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 20);
            this._MenuHelp.Tag = "sqlfwx::menu.help";
            this._MenuHelp.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.Size = new System.Drawing.Size(140, 22);
            this._MenuShowHelp.Tag = "sqlfwx::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help,,,";
            this._MenuShowHelp.Click += new System.EventHandler(this._MenuShowHelp_Click);
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(140, 22);
            this._MenuAbout.Tag = "sqlfwx::menu.about";
            this._MenuAbout.Text = "&About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _TimerRun
            // 
            this._TimerRun.Tick += new System.EventHandler(this._TimerRun_Tick);
            // 
            // _Console
            // 
            this._Console.BackColor = System.Drawing.Color.White;
            this._Console.BgColor = System.ConsoleColor.White;
            this._Console.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Console.EOF = false;
            this._Console.FgColor = System.ConsoleColor.Black;
            this._Console.ForeColor = System.Drawing.Color.Black;
            this._Console.Location = new System.Drawing.Point(0, 24);
            this._Console.Margin = new System.Windows.Forms.Padding(4);
            this._Console.Name = "_Console";
            this._Console.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Both;
            this._Console.Size = new System.Drawing.Size(992, 504);
            this._Console.TabIndex = 0;
            this._Console.WordWrap = false;
            // 
            // FormConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 528);
            this.Controls.Add(this._Console);
            this.Controls.Add(this._MenuMain);
            this.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuMain;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormConsole";
            this.Text = "rqonsole";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConsole_FormClosing);
            this._MenuMain.ResumeLayout(false);
            this._MenuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Robbiblubber.Util.Controls.ConsoleControl _Console;
        private System.Windows.Forms.MenuStrip _MenuMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.Timer _TimerRun;
        private System.Windows.Forms.ToolStripMenuItem _MenuDatabases;
        private System.Windows.Forms.ToolStripMenuItem _MenuConnect;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuCut;
        private System.Windows.Forms.ToolStripMenuItem _MenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _MenuPaste;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem _MenuRun;
        private System.Windows.Forms.ToolStripMenuItem _MenuDisconnect;
        private System.Windows.Forms.ToolStripMenuItem _MenuHistory;
        private System.Windows.Forms.ToolStripSeparator _MenuToolsBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuBrowseHistory;
        private System.Windows.Forms.ToolStripMenuItem _MenuDumpHistory;
        private System.Windows.Forms.ToolStripMenuItem _MenuFavoriteSQL;
        private System.Windows.Forms.ToolStripSeparator _MenuHistoryBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCleanup;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
    }
}

