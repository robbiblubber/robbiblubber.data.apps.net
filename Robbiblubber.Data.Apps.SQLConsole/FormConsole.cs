﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using Robbiblubber.Data.Interpreters;
using Robbiblubber.Data.Interpreters.History;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Data.Apps.SQLConsole
{
    /// <summary>Creates a new instance of this class.</summary>
    public partial class FormConsole: Form, ITargetWindow, IHistoryTargetWindow
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Console is running.</summary>
        private bool _Running = true;
        
        /// <summary>SQL Buffer.</summary>
        private string _Buffer = "";
        
        /// <summary>Provider.</summary>
        private ProviderItem _Provider = null;
        
        /// <summary>SQL interpreter.</summary>
        private SQLInterpreter _Interpreter;

        /// <summary>Show debug flag.</summary>
        private bool _ShowDebug = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormConsole()
        {
            InitializeComponent();

            _LoadSettings();

            Locale.UsingPrefixes("sqlfwx", "utctrl", "debugx");
            Locale.LoadSelection("sqlconsl");
            this.Localize();

            _Interpreter = new SQLInterpreter(this);
            _TimerRun.Enabled = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the debug menu is shown.</summary>
        public bool ShowDebug
        {
            get { return _ShowDebug; }
            set { _ShowDebug = _MenuDebug.Visible = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Method executing shell code.</summary>
        private void _Run()
        {
            while(_Running)
            {
                if(_Buffer == "")
                {
                    _Console.Write("SQL> ");
                }
                else
                {
                    _Console.Write("     ");
                }

                _Buffer += _Console.ReadLine();
                
                if(_Buffer.Trim().EndsWith(";"))
                {
                    _Execute();
                }
            }
        }


        /// <summary>Exits the console.</summary>
        private void _Exit()
        {
            _Running = false;
            _Console.Abort();
        }


        /// <summary>Sets the provider.</summary>
        /// <param name="p">Provider.</param>
        private void _SetProvider(ProviderItem p)
        {
            try
            {
                p.Connect();
                _Provider = p;

                Text = p.Name + " - rqonsole";
            }
            catch(Exception ex)
            {
                DebugOp.Dump("SQLFWX70701", ex);
                _Console.WriteLine("sqlfwx::udiag.connect.failed".Localize("Failed to connect.") + ' ' + ex.Message);
            }
        }


        /// <summary>Opens a database connection.</summary>
        /// <param name="command">Command.</param>
        private void _Open(string command)
        {
            ProviderItem p = FormDatabases.LoadProvider(command.Substring(5).ToLower().Trim().TrimEnd(';').TrimEnd());

            if(p == null)
            {
                _Console.WriteLine("sqlfwx::udiag.connect.notfound".Localize("Database not found."));
            }
            else
            {
                _SetProvider(p);
            }
        }


        /// <summary>Runs a script.</summary>
        /// <param name="file">File name.</param>
        private void _Run(string file)
        {
            _Interpreter.Execute(File.ReadAllText(file));
        }


        /// <summary>Executes SQL.</summary>
        private void _Execute()
        {
            try
            {
                string sql = _Buffer.Trim();
                _Buffer = "";

                if((sql.ToLower() == "exit;") || (sql.ToLower() == "bye;"))
                {
                    Close();
                    return;
                }

                if(sql.ToLower() == "databases;")
                {
                    _MenuDatabases_Click(null, null);
                    return;
                }

                if(sql.ToLower() == "connect;")
                {
                    _MenuDatabases_Click(null, null);
                    return;
                }

                if(sql.ToLower().StartsWith("open "))
                {
                    _Open(sql);
                    return;
                }

                if(_Provider == null)
                {
                    _Console.WriteLine("Not connected.");
                    return;
                }

                if(sql.ToLower().StartsWith("run "))
                {
                    _Run(sql.Substring(4).Trim().TrimEnd(';').Trim());
                    return;
                }

                _Interpreter.Execute(sql);
            }
            catch(Exception ex)
            {
                DebugOp.DumpMessage("SQLFWX70703", ex);
            }
        }


        /// <summary>Saves settings.</summary>
        private void _SaveSettings()
        {
            try
            {
                Ddp cfg = Ddp.Open(PathOp.UserSettingsFile);
                cfg.Set("/settings/debug", ShowDebug);
                cfg.Save();
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX70110", ex); }
        }


        /// <summary>Loads settings.</summary>
        private void _LoadSettings()
        {
            try
            {
                Ddp cfg = Ddp.Open(PathOp.UserSettingsFile);
                ShowDebug = cfg.Get<bool>("/settings/debug");
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX70111", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Timer tick.</summary>
        private void _TimerRun_Tick(object sender, EventArgs e)
        {
            _TimerRun.Enabled = false;
            _Run();
        }


        /// <summary>Form closing.</summary>
        private void FormConsole_FormClosing(object sender, FormClosingEventArgs e)
        {
            _Exit();
        }



        /// <summary>Menu "Connect" click.</summary>
        private void _MenuConnect_Click(object sender, EventArgs e)
        {
            FormConnect f = new FormConnect();

            if(f.ShowDialog() == DialogResult.OK)
            {
                _SetProvider(f.Provider);
            }
        }


        /// <summary>Menu "Databases" click.</summary>
        private void _MenuDatabases_Click(object sender, EventArgs e)
        {
            FormDatabases f = new FormDatabases();

            if(f.ShowDialog() == DialogResult.OK)
            {
                _SetProvider(f.Provider);
            }
        }


        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, EventArgs e)
        {
            FormAbout f = new FormAbout();
            f.ShowDialog();
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Menu "Edit" opening.</summary>
        private void _MenuEdit_DropDownOpening(object sender, EventArgs e)
        {
            _MenuCut.Enabled = _Console.CanCut;
        }


        /// <summary>Menu "Cut" click.</summary>
        private void _MenuCut_Click(object sender, EventArgs e)
        {
            _Console.Cut();
        }


        /// <summary>Menu "Copy" click.</summary>
        private void _MenuCopy_Click(object sender, EventArgs e)
        {
            _Console.Copy();
        }


        /// <summary>Menu "Paste" click.</summary>
        private void _MenuPaste_Click(object sender, EventArgs e)
        {
            _Console.Paste();
        }


        /// <summary>Menu "Help" click.</summary>
        private void _MenuShowHelp_Click(object sender, EventArgs e)
        {
            Process.Start("http://robbiblubber.lima-city.de/wiki.php?title=Help-SQLCLI");
        }


        /// <summary>Menu "Run" click.</summary>
        private void _MenuRun_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "sqlfwx::udiag.filter.all".Localize("All files") + "|*.*";

            if(f.ShowDialog() == DialogResult.OK)
            {
                _Console.Abort();
                _Console.WriteLine();

                _Run(f.FileName);
            }
        }


        /// <summary>Menu "Disconnect" click.</summary>
        private void _MenuDisconnect_Click(object sender, EventArgs e)
        {
            _Provider.Connection.Close();
            _Provider = null;
        }


        /// <summary>Menu "Browse history" click.</summary>
        private void _MenuBrowseHistory_Click(object sender, EventArgs e)
        {
            SQLHistory.BrowseHistory(this);
        }


        /// <summary>Menu "Dump history" click.</summary>
        private void _MenuDumpHistory_Click(object sender, EventArgs e)
        {
            SQLHistory.DumpHistory(this);
        }


        /// <summary>Menu "Favorite SQL" click.</summary>
        private void _MenuFavoriteSQL_Click(object sender, EventArgs e)
        {
            SQLHistory.BrowseHistory(this, true);
        }


        /// <summary>Menu "Clean up history" click.</summary>
        private void _MenuCleanup_Click(object sender, EventArgs e)
        {
            SQLHistory.CleanupHistory(this);
        }


        /// <summary>Menu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            try
            {
                FormSettings f = new FormSettings();

                if(f.ShowDialog() == DialogResult.OK)
                {
                    ShowDebug = f.ShowDebug;
                    _SaveSettings();

                    if(f.SelectedLocale != Locale.Selected)
                    {
                        Locale.Selected = f.SelectedLocale;
                        Locale.SaveSelection("Robbiblubber.Util.NET");
                        this.Localize();
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLFWX70149", ex); }
        }


        /// <summary>Menu "Start/Stop Logging" click.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save Dump" click.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "debugx::udiag.filter.dump".Localize("Debug Dump Files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    DebugOp.CreateDumpFile(d.FileName);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00150", ex); }
        }


        /// <summary>Menu "View Dump" click.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                DebugOp.ShowLog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00151", ex); }
        }


        /// <summary>Menu "Upload Dump" click.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/Robbiblubber.Util.NET/");
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00152", ex); }
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = _MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITargetWindow                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the target view mode.</summary>
        public ViewMode View
        {
            get { return ViewMode.TEXT; }
        }


        /// <summary>Gets the target database provider.</summary>
        public ProviderItem Provider
        {
            get { return _Provider; }
        }


        /// <summary>Gets the target data grid.</summary>
        DataGridView ITargetWindow.Grid
        {
            get { return null; }
        }


        /// <summary>Refreshes the tree view.</summary>
        /// <param name="n">Tree node.</param>
        void ITargetWindow.RefreshTree(TreeNode n)
        {}


        /// <summary>Writes to console.</summary>
        /// <param name="text">Text.</param>
        public void Write(string text)
        {
            _Console.Write(text);
        }


        /// <summary>Gets the selected item.</summary>
        DbItem ITargetWindow.SelectedItem
        {
            get { return null; }
        }


        /// <summary>Gets the selected node.</summary>
        TreeNode ITargetWindow.SelectedNode
        {
            get { return null; }
        }


        /// <summary>Writes to grid.</summary>
        /// <param name="text">Text.</param>
        void ITargetWindow.WriteToGrid(string text)
        {}


        /// <summary>Sets grid data source.</summary>
        /// <param name="dataSource">Data source.</param>
        void ITargetWindow.DataToGrid(object dataSource)
        {}


        /// <summary>Shows the wait window.</summary>
        void ITargetWindow.ShowWait()
        {}


        /// <summary>Shows the wait window.</summary>
        /// <param name="text">Text.</param>
        void ITargetWindow.ShowWait(string text)
        {}


        /// <summary>Hides the wait window.</summary>
        void ITargetWindow.HideWait()
        {}


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        void ITargetWindow.ExecuteSynchronized(ThreadStart c)
        {
            Thread t = new Thread(c);
            t.Start();
            t.Join();
        }


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Paramter.</param>
        void ITargetWindow.ExecuteSynchronized(ParameterizedThreadStart c, object p)
        {
            Thread t = new Thread(c);
            t.Start(p);
            t.Join();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IHistoryTargetWindow                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Applies a text from history.</summary>
        /// <param name="text">Text.</param>
        void IHistoryTargetWindow.Apply(string text)
        {
            _Console.Paste(text);
        }
    }
}
