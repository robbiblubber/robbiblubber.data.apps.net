﻿using System;
using System.Threading;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util;



namespace Robbiblubber.Data.Apps.SQLConsole
{
    /// <summary>Program class.</summary>
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Main form.</summary>
        internal static FormConsole __Main;

        /// <summary>Splash form.</summary>
        internal static FormSplash __Splash;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            PathOp.ApplicationPart = "robbiblubber.org/SQLConsole";
            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");

            __Splash = new FormSplash();
            __Splash.Show();
            Application.DoEvents();

            Thread.Sleep(1600);

            __Main = new FormConsole();
            __Splash.DelayClose();
            Application.Run(__Main);
        }
    }
}
